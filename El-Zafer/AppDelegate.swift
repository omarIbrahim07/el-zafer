//
//  AppDelegate.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/7/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UIKit
import UserNotifications
import Firebase
import GoogleMaps


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    var orderId: Int?
    var orderPrice: Float?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        checkedForLoggedInUser()
        IQKeyboardManager.shared.enable = true
        
        FirebaseApp.configure()
        getFirebaseToken()
        
        GMSServices.provideAPIKey("AIzaSyCa5Goe4I7WunAA_gdB13SM4DWTEtYeYwM")
//        GMSPlacesClient.provideAPIKey("AIzaSyCa5Goe4I7WunAA_gdB13SM4DWTEtYeYwM")

        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- Helpers
    func checkedForLoggedInUser() {
        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
            window!.rootViewController = viewController
            window!.makeKeyAndVisible()
        }
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
//         Messaging.messaging().apnsToken = deviceToken

        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func getFirebaseToken() {
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let resultValue = result {
                FirebaseToken = resultValue.token
                print("Remote instance ID token: \(resultValue.token)")
            }
        })
    }
    
    


}

//
//  Copyright (c) 2016 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        //*****************************************************
//        let userInfo = notification.request.content.userInfo
//
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//        // Messaging.messaging().appDidReceiveMessage(userInfo)
//        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//
//        // Print full message.
//        print("^^^userInfo^^^ : \(userInfo)")
//        print("دخلت النوتيفكيشن")
//
//        if let aps = userInfo["aps"] as? NSDictionary {
//            if let orderId = userInfo["order_id"] as? NSString, let priceCalculated = userInfo["price"] as? NSString {
//
//                self.orderId = Int(orderId as String)
//                self.orderPrice = Float(priceCalculated as String)
//
////                print("myorderID = \(orderId) & the price is: \(priceCalculated)")
//                print("myorderID = \(self.orderId) & the price is: \(self.orderPrice)")
//
//
//
//                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//                if let viewController = storyboard.instantiateViewController(withIdentifier: "PriceCalculatedViewController") as? PriceCalculatedViewController {
//                    viewController.packageId = self.orderId
//                    viewController.calculatedPrice = self.orderPrice
//                    window!.rootViewController = viewController
//                    window!.makeKeyAndVisible()
//                }
//
//            }
//            if let notification = aps["alert"] as? NSDictionary {
//                print("دخلت هنا")
//            }
//        }
        //*****************************************************
        
        // for sending from firebase , i receive alert as NSString
        ////////////////////////////////////////////////////////////////////
//        if let aps = userInfo["aps"] as? NSDictionary {
//            if let alert = aps["alert"] as? NSDictionary {
//                if let message = alert["badge"] as? NSString {
//                    //Do stuff
//                    print("Message: \(message)")
//                }
//            } else if let alert = aps["alert"] as? NSString {
//                //Do stuff
//                print("Alert: \(alert)")
//            }
//        }
        ///////////////////////////////////////////////////////////////////
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // access the payload from firebase
        let userInfo = response.notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("^^^userInfo^^^ : \(userInfo)")
        
        // access the data from payload dictionary
        if let aps = userInfo["aps"] as? NSDictionary {
            if let orderId = userInfo["order_id"] as? NSString, let priceCalculated = userInfo["price"] as? NSString {
                self.orderId = Int(orderId as String)
                self.orderPrice = Float(priceCalculated as String)
                
                print("myorderID = \(self.orderId) & the price is: \(self.orderPrice)")
                
                // go to price calculated viewcontroller after pressing on the notification to show the price of stored package
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                if let viewController = storyboard.instantiateViewController(withIdentifier: "PriceCalculatedViewController") as? PriceCalculatedViewController {
                    viewController.packageId = self.orderId
                    viewController.calculatedPrice = self.orderPrice
                    window!.rootViewController = viewController
                    window!.makeKeyAndVisible()
                }
                
            }
            if let notification = aps["alert"] as? NSDictionary {
                print("دخلت هنا")
            }
        }
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

