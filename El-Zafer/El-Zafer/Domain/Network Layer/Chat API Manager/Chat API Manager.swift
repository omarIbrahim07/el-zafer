//
//  Chat API Manager.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/25/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import Foundation
import ObjectMapper

class ChatAPIManager: BaseAPIManager {

    func getMessages(basicDictionary params:APIParams , onSuccess: @escaping ([Message])->Void, onFailure: @escaping  (APIError)->Void) {
        
//        let get_Saved_Advertisements = "\(GET_SAVED_ADVERTISEMENTS_URL)\(userID)"
        
        let engagementRouter = BaseRouter(method: .post, path: GET_MESSAGES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Messeges"] as? [[String : Any]] {
                let wrapper = Mapper<Message>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getFirstMessagesChat(basicDictionary params:APIParams, onSuccess: @escaping (Message) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: GET_CHAT_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["Selected Message"] as? [String : Any], let wrapper = Mapper<Message>().map(JSON: data) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getChatMessages(basicDictionary params:APIParams , onSuccess: @escaping ([Message])->Void, onFailure: @escaping  (APIError)->Void) {
                
        let engagementRouter = BaseRouter(method: .post, path: GET_CHAT_MESSAGES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["Replies"] as? [[String : Any]] {
                let wrapper = Mapper<Message>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func textChatMessage(basicDictionary params:APIParams, onSuccess: @escaping (ReturnedMessage) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: TEXT_CHAT_MESSAGE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any] ,let wrapper = Mapper<ReturnedMessage>().map(JSON: response) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

}
