//
//  User.swift
//  Blabber
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    
    var id : Int?
    var firstName : String?
    var lastName : String?
    var email : String?
    var phone : String?
    var mobile1 : String?
    var mobile2 : String?
    var mobile3 : String?
    var image : String?
    
    required override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        phone <- map["phone"]
        mobile1 <- map["mobile1"]
        mobile2 <- map["mobile2"]
        mobile3 <- map["mobile3"]
        image <- map["image"]
    }
    
    //MARK: - NSCoding -
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        self.lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.mobile1 = aDecoder.decodeObject(forKey: "mobile1") as? String
        self.mobile2 = aDecoder.decodeObject(forKey: "mobile2") as? String
        self.mobile3 = aDecoder.decodeObject(forKey: "mobile3") as? String
        self.image = aDecoder.decodeObject(forKey: "image") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(firstName, forKey: "first_name")
        aCoder.encode(lastName, forKey: "last_name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(mobile1, forKey: "mobile1")
        aCoder.encode(mobile2, forKey: "mobile2")
        aCoder.encode(mobile3, forKey: "mobile3")
        aCoder.encode(image, forKey: "image")
    }
    
}
