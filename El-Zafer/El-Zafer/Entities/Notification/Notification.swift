//
//  Notification.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 7/29/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Notificationn: Mappable {
    
    var orderId : String!
    var from : String!
    var to : String!
    var createdAt : String!
    var price : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        orderId <- map["order_id"]
        from <- map["loc_from"]
        to <- map["loc_to"]
        createdAt <- map["created_at"]
        price <- map["price"]
    }
    
}
