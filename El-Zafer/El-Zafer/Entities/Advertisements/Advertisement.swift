//
//  Advertisement.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 4/7/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import Foundation
import ObjectMapper

class Categories : Mappable {
    
    var categories : [Category]?
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        categories <- map["Ads"]
    }
    
}

class SpecialCategories : Mappable {
    
    var specialCategories : [Category]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        specialCategories <- map["AllFeaturedAds"]
    }
    
}

//class Category: Mappable {
//
//    var categoryAdvertisements : [AdvertisementDetailsModel]?
//    var categoryName : String?
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//        categoryAdvertisements <- map["cat_ads"]
//        categoryName <- map["cat_name"]
//    }
//
//}

class AdvertisementDetailsModel: Mappable {
    
    var id : Int!
    var countryID : String!
    var userId : String!
    var categoryId : String!
    var title : String!
    var description : String!
    var tags : String!
    var price : String!
    var negotiable : String!
    var contactName : String!
    var email : String!
    var phone : String!
    var address : String!
    var cityID : String!
    var city : String!
    var visits : String!
    var advertisementTime : String!
    var advertisementImage : String!
    var createdAt : String!
    var archived : String!
    var advertisementImages : [Picture]!
    var advertisementBanner : String!
    
//    var name : String!
//    var image : String!
//    var parent : String!

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        countryID <- map["country_code"]
        userId <- map["user_id"]
        categoryId <- map["category_id"]
        title <- map["title"]
        description <- map["description"]
        tags <- map["tags"]
        price <- map["price"]
        negotiable <- map["negotiable"]
        contactName <- map["contact_name"]
        email <- map["email"]
        phone <- map["phone"]
        address <- map["address"]
        cityID <- map["city_id"]
        city <- map["city"]
        visits <- map["visits"]
        advertisementTime <- map["archived_at.date"]
        createdAt <- map["created_at_ta"]
        archived <- map["archived"]
        advertisementImage <- map["picture"]
        advertisementImages <- map["picture"]
        advertisementBanner <- map["ad_banner"]
    }
    
}

class Picture: Mappable {
    
    var id : Int!
    var postId : String!
    var image : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        postId <- map["post_id"]
        image <- map["filename"]
    }
    
}










//struct PostModel: Mappable {
//
//    var id : Int!
//    var userID : String!
//    var interestID : String!
//    var countryID : String!
//    var description : String!
//    var viewCount : String!
//    var commentCount : String!
//    var likeCount : String!
//    var feedBackCount : String!
//    var isActive : String!
//    var createdAt : String!
//    var updatedAt : String!
//    var countryName : String!
//    var interestName : String!
//    var userName : String!
//    var userImage : String!
//    var isFollow : Int!
//    var isLike : Int!
//    var comments : [Comment]!
//
//    init?(map: Map) {
//
//    }
//
//    mutating func mapping(map: Map) {
//        id <- map["id"]
//        userID <- map["user_id"]
//        interestID <- map["interest_id"]
//        countryID <- map["country_id"]
//        description <- map["description"]
//        viewCount <- map["view_count"]
//        commentCount <- map["comment_count"]
//        likeCount <- map["like_count"]
//        feedBackCount <- map["feedback_count"]
//        isActive <- map["is_active"]
//        createdAt <- map["created_at"]
//        updatedAt <- map["updated_at"]
//        countryName <- map["country_name"]
//        interestName <- map["interest_name"]
//        userName <- map["user_name"]
//        userImage <- map["user_image"]
//        isFollow <- map["is_follow"]
//        isLike <- map["is_like"]
//        comments <- map["comments"]
//    }
//}

struct Comment: Mappable, Equatable {
    
    var id : Int!
    var userID : Int!
    var userName : String!
    var userImage : String!
    var userComment : String!
    var likeComment : Int!
    var isLikeComment : Int!
    var createdAt : String!
    var replies: [Comment]?
    
    var isReply: Bool = false
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        userID <- map["user_id"]
        userName <- map["user_name"]
        userImage <- map["user_image"]
        userComment <- map["comment"]
        likeComment <- map["like_count"]
        isLikeComment <- map["is_like"]
        createdAt <- map["created_at"]
        replies <- map["replies"]
    }
    
    static func == (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.id == rhs.id
    }
}

