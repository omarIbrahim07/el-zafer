//
//  Categories.swift
//  Advertising808080
//
//  Created by Omar Ibrahim on 3/12/19.
//  Copyright © 2019 Advertising808080. All rights reserved.
//

import Foundation
import ObjectMapper

class MainCategories: Mappable {
    
    var id : Int!
    var name : String!
    var image : String!
//    var postsCount : Int!
    var parent : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        image <- map["picture"]
//        postsCount <- map["count"]
        parent <- map["parent_id"]
    }
    
}


class SubCategories: Mappable {
    
    var id : Int!
    var name : String!
    var image : String!
//    var postsCount : Int!
    var parent : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        image <- map["picture"]
//        postsCount <- map["count"]
        parent <- map["parent_id"]
    }
}

class Country: Mappable {
    
    var code : String!
    var name : String!
    var image : String!

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        code <- map["code"]
        name <- map["asciiname"]
        image <- map["background_image"]
    }
}

class City: Mappable {
    
    var id : Int!
    var name : String!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["asciiname"]
    }
}

class PostType: Mappable {
    
    var id : Int!
    var name : String!
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
}


class SecondaryCategories: Mappable {
    
    var id : Int!
    var countryID : String!
    var userId : String!
    var categoryId : String!
    var title : String!
    var description : String!
    var tags : String!
    var price : String!
    var negotiable : String!
    var contactName : String!
    var email : String!
    var phone : String!
    var address : String!
    var cityID : String!
    var visits : String!
    var advertisementTime : String!
    //    var name : String!
    //    var image : String!
    //    var parent : String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        countryID <- map["country_code"]
        userId <- map["user_id"]
        categoryId <- map["category_id"]
        title <- map["title"]
        description <- map["description"]
        tags <- map["tags"]
        price <- map["price"]
        negotiable <- map["negotiable"]
        contactName <- map["contact_name"]
        email <- map["email"]
        phone <- map["phone"]
        address <- map["address"]
        cityID <- map["city_id"]
        visits <- map["visits"]
        advertisementTime <- map["archived_at.date"]
        
        //        name <- map["name"]
        //        image <- map["picture"]
        //        parent <- map["parent_id"]
    }
}


