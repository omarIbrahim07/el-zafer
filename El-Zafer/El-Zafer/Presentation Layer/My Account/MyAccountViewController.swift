//
//  MyAccountViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class MyAccountViewController: BaseViewController {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        userImage.addCornerRadius(raduis: userImage.frame.height / 2, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
        editView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
        
        if let image = UserDefaultManager.shared.currentUser?.image {
            let imageURL = ImageURL + image
            userImage.loadImageFromUrl(imageUrl: imageURL)
        }
        
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "PackageDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "PackageDetailsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func editAccountInfo() {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "EditAccountViewController") as! EditAccountViewController
        //        viewController.postID = postID
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func editProfileButtonIsPressed(_ sender: Any) {
        print("Edit is pressed")
        editAccountInfo()
    }
    
}

extension MyAccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: PackageDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PackageDetailsTableViewCell") as? PackageDetailsTableViewCell {
            
            if indexPath.row == 0 {
                cell.packageDetailsTitleLabel.text = "الإسم"
                cell.packageDetailsDescriptionLabel.text = UserDefaultManager.shared.currentUser?.firstName
            } else if indexPath.row == 1 {
                cell.packageDetailsTitleLabel.text = "رقم الهاتف"
                cell.packageDetailsDescriptionLabel.text = UserDefaultManager.shared.currentUser?.mobile1
            } else if indexPath.row == 2 {
                cell.packageDetailsTitleLabel.text = "البريد الإلكتروني"
                cell.packageDetailsDescriptionLabel.text = UserDefaultManager.shared.currentUser?.email
                cell.view.isHidden = true
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}
