//
//  AboutViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController {
    
    var about: [About] = []

    @IBOutlet weak var aboutAppLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutApp()
        // Do any additional setup after loading the view.
    }
    
    func aboutApp() {
        
        self.startLoading()
        
        AboutAPIManager().getAboutApp(basicDictionary: [:], onSuccess: { (about) in
            self.about = about
            self.aboutAppLabel.text = self.about[0].content
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
    

}
