//
//  ExtraServiceTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/9/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

//protocol extraServicesButtonTableViewCellDelegate {
//    func didExtraServicesButtonPressed(tag: Int, extraServiceBool: Bool)
//}
protocol extraServicesButtonTableViewCellDelegate {
    func didExtraServicesButtonPressed(tag: Int, extraServicesChecked: Bool)
}

class ExtraServiceTableViewCell: UITableViewCell {
        
    var extraServicesDelegate: extraServicesButtonTableViewCellDelegate?
    
//    var tagArray: [Bool] = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]
    var tagArray: [Bool] = []
    
    var viewModelsExtraServices : [AdditionalService] = []{
        didSet {
            getTagArray()
        }
    }

    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var extraServiceTitleLabel: UILabel!
    @IBOutlet weak var extraServicePriceLabel: UILabel!
    @IBOutlet weak var extraServiceCurrencyLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        getAdditionalServices()
        // Initialization code
    }
    
    func getAdditionalServices() {
        
//        self.startLoading()
        
        PackageAPIManager().getAdditionalServices(basicDictionary: [:], onSuccess: { (additionalServices) in
            
            self.viewModelsExtraServices = additionalServices
            
//            self.tableView.reloadData()
//            self.stopLoadingWithSuccess()
            
        }) { (error) in
//            self.stopLoadingWithError(error: error)
        }
        
    }
    
    func getTagArray() {
        
        for extraService in self.viewModelsExtraServices {
            self.tagArray.append(false)
        }
        print(tagArray)
    }
    
    //MARK:- Delegate Helpers
//    func didExtraServicesButtonPressed(tag: Int, extraServiceBool: Bool) {
//        if let delegateValue = extraServicesDelegate {
//            delegateValue.didExtraServicesButtonPressed(tag: tag, extraServiceBool: extraServiceBool)
//        }
//    }
    func didExtraServicesButtonPressed(tag: Int, extraServiceChecked: Bool) {
        if let delegateValue = extraServicesDelegate {
            delegateValue.didExtraServicesButtonPressed(tag: tag, extraServicesChecked: extraServiceChecked)
        }
    }

    @IBAction func checkedIsPressed(_ sender: Any) {
        
        if tagArray[checkButton.tag] == false {
            checkBoxImage.image = UIImage(named: "Checked")
            tagArray[checkButton.tag] = true
            print("mobile service checked")
        } else if tagArray[checkButton.tag] == true {
            checkBoxImage.image = UIImage(named: "01_0004_Rounded-Rectangle-8")
            tagArray[checkButton.tag] = false
            print("mobile service unchecked")
        }
        didExtraServicesButtonPressed(tag: checkButton.tag, extraServiceChecked: tagArray[checkButton.tag])
        print(tagArray)
        
//        if checkButton.tag == 0 {
//            if mobileMessagesChecked == false {
//                checkBoxImage.image = UIImage(named: "Checked")
//                mobileMessagesChecked = true
//                print("mobile service checked")
//            } else if mobileMessagesChecked == true {
//                checkBoxImage.image = UIImage(named: "01_0004_Rounded-Rectangle-8")
//                mobileMessagesChecked = false
//                print("mobile service unchecked")
//            }
//            didExtraServicesButtonPressed(tag: checkButton.tag, extraServiceBool: mobileMessagesChecked)
//        } 

    }
    
    
}
