//
//  ExtraServicesViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/9/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class ExtraServicesViewController: BaseViewController {
    
    var additionalServices: [AdditionalService] = []
    
    var window: UIWindow?
    
    var postId: Int?
    var storable: Int?
    
    var tagarray: [String] = []
    var tagarrayedited: [String] = []
//    var tagarrayedited: [String]?

    var packagePrice: Float?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendPackageView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "خدمات إضافية"
        configureView()
        configureTableView()
        getAdditionalServices()
        
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        sendPackageView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "ExtraServiceTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraServiceTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func getAdditionalServices() {
        
        self.startLoading()
        
        PackageAPIManager().getAdditionalServices(basicDictionary: [:], onSuccess: { (additionalServices) in
            
            self.stopLoadingWithSuccess()
            
        self.additionalServices = additionalServices
        
        for extraService in self.additionalServices {
            self.tagarray.append("0")
        }
        print(self.tagarray)
        
        self.tableView.reloadData()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }

    func goToPrice() {

        if self.storable == 1 {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "PackageSentViewController") as! PackageSentViewController
            //        viewController.postID = postID
            navigationController?.pushViewController(viewController, animated: true)
        } else if self.storable == 0 {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "PriceCalculatedViewController") as! PriceCalculatedViewController
            viewController.calculatedPrice = self.packagePrice
            viewController.packageId = self.postId
            navigationController?.pushViewController(viewController, animated: true)
            ////////
//            if let viewController = storyboard!.instantiateViewController(withIdentifier: "PriceCalculatedViewController") as? PriceCalculatedViewController {
//                viewController.calculatedPrice = self.packagePrice
//                viewController.packageId = self.postId
//                window!.rootViewController = viewController
//                window!.makeKeyAndVisible()
//            }
            
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let viewController = storyboard.instantiateViewController(withIdentifier: "PriceCalculatedViewController") as? PriceCalculatedViewController
//            viewController!.calculatedPrice = self.packagePrice
//            viewController!.packageId = self.postId
//            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
    }

    @IBAction func sendPackageIsPressed(_ sender: Any) {
        
        guard let orderId = self.postId, orderId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الخدمة"
            showError(error: apiError)
            return
        }
        print(orderId)
        
        /************ Before editing we send the whole array ********************/
//        let parameters: [String : AnyObject] = [
//            "order_id" : self.postId as AnyObject,
//            "services[]" : self.tagarray as AnyObject
//        ]
        /***********************************************/
        
        
        /************ After editing as Waref needs to send just the choosed IDs of extra services only ********************/
        self.tagarrayedited = []
        for n in 0...tagarray.count - 1 {
            if tagarray[n] != "0" {
                tagarrayedited.append(tagarray[n])
            }
        }
        print("Array after editing is: \(tagarrayedited)")

        if tagarrayedited.count == 0 {
            
            let parameters: [String : AnyObject] = [
                "order_id" : self.postId as AnyObject,
            ]
            
            weak var weakSelf = self
            
            startLoading()
            
            PackageAPIManager().addExtraServices(basicDictionary: parameters, onSuccess: { (packagePrice) in
                
            self.stopLoadingWithSuccess()
                
            self.packagePrice = Float(packagePrice)
                
                self.goToPrice()
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        }
//            else {
//
//                    let parameters: [String : AnyObject] = [
//                        "order_id" : self.postId as AnyObject,
//                        "services[]" : self.tagarrayedited as AnyObject
//                    ]
//
//                    weak var weakSelf = self
//
//                    startLoading()
//
//                    PackageAPIManager().addExtraServices(basicDictionary: parameters, onSuccess: { (packagePrice) in
//
//                        self.stopLoadingWithSuccess()
//
//                        self.packagePrice = Float(packagePrice)
//
//                        self.goToPrice()
//
//                    }) { (error) in
//                        weakSelf?.stopLoadingWithError(error: error)
//                    }
//                }

            else if tagarrayedited.count == 1 {

            let parameters: [String : AnyObject] = [
                "order_id" : self.postId as AnyObject,
                "services[]" : self.tagarrayedited as AnyObject
            ]

            weak var weakSelf = self

            startLoading()

            PackageAPIManager().addExtraServicesFloat(basicDictionary: parameters, onSuccess: { (packagePrice) in

                self.stopLoadingWithSuccess()

                self.packagePrice = Float(packagePrice)

                self.goToPrice()

            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }

        } else {

            let parameters: [String : AnyObject] = [
                "order_id" : self.postId as AnyObject,
                "services[]" : self.tagarrayedited as AnyObject
            ]

            weak var weakSelf = self

            startLoading()

            PackageAPIManager().addExtraServicesFloat(basicDictionary: parameters, onSuccess: { (packagePrice) in

                self.stopLoadingWithSuccess()

                self.packagePrice = Float(packagePrice)

                self.goToPrice()

            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        }
    }

}

extension ExtraServicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return additionalServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: ExtraServiceTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ExtraServiceTableViewCell") as? ExtraServiceTableViewCell {
            cell.extraServiceTitleLabel.text = additionalServices[indexPath.row].name
            cell.extraServicePriceLabel.text = additionalServices[indexPath.row].price
//            cell.tag = indexPath.row
            cell.viewModelsExtraServices = self.additionalServices
            cell.checkButton.tag = indexPath.row
            cell.extraServicesDelegate = self
            
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension ExtraServicesViewController: extraServicesButtonTableViewCellDelegate {
    
    func didExtraServicesButtonPressed(tag: Int, extraServicesChecked: Bool) {

        if extraServicesChecked == true {
            self.tagarray[tag] = "\(tag + 1)"
        } else if extraServicesChecked == false {
            self.tagarray[tag] = "0"
        }
        print(tagarray)
        
    }
    
}
