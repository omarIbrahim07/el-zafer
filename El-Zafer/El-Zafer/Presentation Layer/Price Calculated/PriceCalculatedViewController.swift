//
//  PriceCalculatedViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/9/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class PriceCalculatedViewController: BaseViewController {
    
    var window: UIWindow?
    
    var calculatedPrice: Float?
    var packageId: Int?
    
    @IBOutlet weak var calculatedPriceLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        navigationItem.title = "تأكيد الشُحنة"
        print(packageId)
        if let price = calculatedPrice {
            calculatedPriceLabel.text = "\(price)"
        }
        
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        confirmButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        backButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func goToHome() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationViewController")
        window!.rootViewController = viewController
        window!.makeKeyAndVisible()
    }
    
    func goToEditedHome() {
        performSegue(withIdentifier: "unwindToVC1WithSegue", sender: self)

    }
    
    
    func goToMyPackages() {
        if let myPackagesNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyPackagesNavigationViewController") as? UINavigationController, let rootViewContoller = myPackagesNavigationController.viewControllers[0] as? MyPackagesViewController {
            //                rootViewContoller.test = "test String"
            self.present(myPackagesNavigationController, animated: true, completion: nil)
            print("Fes")
        }
    }
    
    func confirmOrder() {
        
        guard let orderId = self.packageId, orderId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الشُحنة"
            showError(error: apiError)
            return
        }
        
        let orderStatus = "2"
        
        let parameters = [
            "orderStatus_id" : orderStatus as AnyObject,
            "order_id" : self.packageId as AnyObject,
        ]
        
        weak var weakSelf = self

        startLoading()
        
        PackageAPIManager().confirmOrder(basicDictionary: parameters, onSuccess: { () in
            
            self.stopLoadingWithSuccess()

            print("موشكرين")
            self.showDonePopUp()
//            self.goToHomePage()
//            self.navigationController?.popToRootViewController(animated: true)
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    
    
    
    func saveToMyPackages() {
    
        guard let orderId = self.packageId, orderId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الشُحنة"
            showError(error: apiError)
            return
        }
        
        let ostatus = "1"
        
        let params = [
            "ostatus" : ostatus as AnyObject,
            "order_id" : self.packageId as AnyObject,
            ]
        
        weak var weakSelf = self
        
        startLoading()
        PackageAPIManager().addPackage(basicDictionary: params, onSuccess: { (packageIdd) in
            
            print(packageIdd)
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "تم إضافة الشُحنة إلى شُحناتي !", message: "الرجوع إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.saveToMyPackages()
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showBackPopUp() {
        let alertController = UIAlertController(title: "لم يتم تأكيد الشُحنة", message: "الرجوع إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @IBAction func confirmButtonIsPressed(_ sender: Any) {
        confirmOrder()
        print("Confirm")
    }
    
    @IBAction func backButtonIsPressed(_ sender: Any) {
        showBackPopUp()
    }
    

}
