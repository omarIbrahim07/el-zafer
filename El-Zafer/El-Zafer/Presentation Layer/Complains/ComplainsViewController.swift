//
//  ComplainsViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/13/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class ComplainsViewController: BaseViewController {

    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var complainTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        userNameTextField.endEditing(true)
        emailAddressTextField.endEditing(true)
        complainTextView.endEditing(true)

    }
    
    func configureView() {
        complainTextView.addCornerRadius(raduis: 12, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        sendView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
    }
    
    func giveFeedback() {
        
        guard let name = userNameTextField.text, name.count > 0 else {
            let apiError = APIError()
            apiError.message = "من فضلك ادخل إسمك"
            showError(error: apiError)
            return
        }
        
        guard let email = emailAddressTextField.text, email.count > 0 else {
            let apiError = APIError()
            apiError.message = "من فضلك ادخل البريد الإلكتروني"
            showError(error: apiError)
            return
        }
        
        guard let feedback = complainTextView.text, feedback.count > 0 else {
            let apiError = APIError()
            apiError.message = "من فضلك ادخل تعليقك"
            showError(error: apiError)
            return
        }
        
        let parameters = [
            "name" : name as AnyObject,
            "email" : email as AnyObject,
            "message" : feedback as AnyObject
        ]
        
        startLoading()
        FeedbacksAPIManager().giveFeedback(basicDictionary: parameters, onSuccess: { (sent) in
            
            self.stopLoadingWithSuccess()
            self.showDonePopUp()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "لقد تم التقييم!", message: "شكرًا لتقييمك", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @IBAction func sendButtonIsPressed(_ sender: Any) {
        giveFeedback()
        print("Send")
    }
    
}

extension ComplainsViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ComplainsViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
