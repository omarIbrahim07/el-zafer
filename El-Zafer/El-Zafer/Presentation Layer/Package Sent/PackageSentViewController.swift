//
//  PackageSentViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/9/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class PackageSentViewController: BaseViewController {

    @IBOutlet weak var myPackagesView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        myPackagesView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func goToMyPackages() {
        if let myPackagesNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyPackagesNavigationViewController") as? UINavigationController, let rootViewContoller = myPackagesNavigationController.viewControllers[0] as? MyPackagesViewController {
            //                rootViewContoller.test = "test String"
            self.present(myPackagesNavigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func homeButtonIsPressed(_ sender: Any) {
        print("Pressed")
        self.goToHomePage()
    }
}
