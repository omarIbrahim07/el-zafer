//
//  EvaluationsViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/13/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import Cosmos

class EvaluationsViewController: BaseViewController {
    
    var idSelected: Int?
    
    var rateArray: [String] = []
    
    var feedbackItems: [FeedbackItem] = []
    var myPackages: [Package] = []

    @IBOutlet weak var evaluateButton: UIButton!
    @IBOutlet weak var idTextField: UITextField!
    @IBOutlet weak var evaluationTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        getMyPackages()
        configureTableView()
        getEvaluationItems()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        idTextField.endEditing(true)
    }
    
    func configureView() {
        evaluateButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        createPicker(textField: idTextField, tag: 0)
    }
    
    func configureTableView() {
        evaluationTableView.register(UINib(nibName: "EvaluationTableViewCell", bundle: nil), forCellReuseIdentifier: "EvaluationTableViewCell")
        evaluationTableView.dataSource = self
        evaluationTableView.delegate = self
        evaluationTableView.reloadData()
    }
    
    func getEvaluationItems() {
        
        self.startLoading()
        
        PackageAPIManager().getFeedbackElements(basicDictionary: [:], onSuccess: { (feedbackItems) in
            
        self.stopLoadingWithSuccess()
        self.feedbackItems = feedbackItems
            
        for extraService in self.feedbackItems {
            self.rateArray.append("5.0")
        }
            
        self.evaluationTableView.reloadData()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
    func getMyPackages() {
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().getMyPackages(basicDictionary: [:], onSuccess: { (packages) in
            
            self.stopLoadingWithSuccess()
            self.myPackages = packages
            print(self.myPackages)
            
        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
            self.showNoPackagePopUp()
        }
    }
    
    func createPicker(textField: UITextField,tag: Int) {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        textField.inputView = Picker
        Picker.tag = tag
    }
    
    func evaluateService() {
        
        guard let orderId = idTextField.text, orderId.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الخدمة"
            showError(error: apiError)
            return
        }
        print(orderId)
        
        let parameters: [String : AnyObject] = [
            "order_id" : orderId as AnyObject,
            "ratings[]" : self.rateArray as AnyObject
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().evaluateServices(basicDictionary: parameters, onSuccess: { (_) in
            
            self.stopLoadingWithSuccess()
            print("7obby")
            self.showDonePopUp()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
        
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "لقد تم التقييم!", message: "شكرًا لتقييمك", preferredStyle: .alert)
//        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showNoPackagePopUp() {
        let alertController = UIAlertController(title: "عُذرًا لا يوجد لديك شُحنات !", message: "يمكنك التقييم بعد طلب شُحنة", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "إغلاق", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
//        window!.makeKeyAndVisible()
        
//        if let homeNavigationController = storyboard?.instantiateViewController(withIdentifier: "SignInNavigationController") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {
//
//        }
    }
    

    
    @IBAction func evaluateButtonIsPressed(_ sender: Any) {
        print("Evaluate")
        evaluateService()
    }
    
    
}

extension EvaluationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedbackItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: EvaluationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "EvaluationTableViewCell") as? EvaluationTableViewCell {
            
            cell.ratingLabel.text = feedbackItems[indexPath.row].name
            cell.ratingView.tag = feedbackItems[indexPath.row].id
            cell.viewModelsfeedbackItems = self.feedbackItems
            cell.delegate = self
            
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension EvaluationsViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension EvaluationsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return myPackages.count > 0 ? 1 : 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return myPackages.count > 0 ? myPackages.count : 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            idTextField.text = "\(myPackages[0].id ?? 0)"
            idSelected = myPackages[0].id
            return "\(myPackages[row].id ?? 0)"
        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            idTextField.text = "\(myPackages[row].id ?? 0)"
            idSelected = myPackages[row].id
        }
        //            self.view.endEditing(true)
    }
    
}


extension EvaluationsViewController: feedBackTableViewCellDelegate {
    
    func didfeedBackViewRated(tag: Int, rate: Float) {
        self.rateArray[tag] = String(rate)
        print(self.rateArray)
    }
    
}

////
////  EvaluationsViewController.swift
////  El-Zafer
////
////  Created by Omar Ibrahim on 5/13/19.
////  Copyright © 2019 Egy designer. All rights reserved.
////
//
//import UIKit
//import Cosmos
//
//class EvaluationsViewController: BaseViewController {
//
//    var disciplineRate: Any?
//    var rateArray: [Double] = [1,1,1,1,1,1,1,1]
//
//    @IBOutlet weak var evaluateButton: UIButton!
//    @IBOutlet weak var cancelButton: UIButton!
//    @IBOutlet weak var disciplineCosmosView: CosmosView!
//    @IBOutlet weak var orderSpeedCosmosView: CosmosView!
//    @IBOutlet weak var exactTimeCosmosView: CosmosView!
//    @IBOutlet weak var clearRequirementsCosmosView: CosmosView!
//    @IBOutlet weak var clearReplies: CosmosView!
//    @IBOutlet weak var easyToUseCosmosView: CosmosView!
//    @IBOutlet weak var easyToArriveCosmosView: CosmosView!
//    @IBOutlet weak var totalServiceCosmosView: CosmosView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        configureView()
//        rate()
//        //        getEvaluationItems()
//        // Do any additional setup after loading the view.
//    }
//
//    func configureView() {
//        evaluateButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        cancelButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//    }
//
//    func rate() {
//        disciplineCosmosView.didTouchCosmos = { rating in
//            self.rateArray[0] = self.disciplineCosmosView.rating
//            print(self.rateArray[0])
//        }
//        orderSpeedCosmosView.didTouchCosmos = { rating in
//            self.rateArray[1] = self.orderSpeedCosmosView.rating
//            print(self.rateArray[1])
//        }
//        exactTimeCosmosView.didTouchCosmos = { rating in
//            self.rateArray[2] = self.exactTimeCosmosView.rating
//            print(self.rateArray[2])
//        }
//        clearRequirementsCosmosView.didTouchCosmos = { rating in
//            self.rateArray[3] = self.clearRequirementsCosmosView.rating
//            print(self.rateArray[3])
//        }
//        clearReplies.didTouchCosmos = { rating in
//            self.rateArray[4] = self.clearReplies.rating
//            print(self.rateArray[4])
//        }
//        easyToUseCosmosView.didTouchCosmos = { rating in
//            self.rateArray[5] = self.easyToUseCosmosView.rating
//            print(self.rateArray[5])
//        }
//        easyToArriveCosmosView.didTouchCosmos = { rating in
//            self.rateArray[6] = self.easyToArriveCosmosView.rating
//            print(self.rateArray[6])
//        }
//        totalServiceCosmosView.didTouchCosmos = { rating in
//            self.rateArray[7] = self.totalServiceCosmosView.rating
//            print(self.rateArray[7])
//        }
//    }
//
//    func getEvaluationItems() {
//
//        self.startLoading()
//
//        PackageAPIManager().getFeedbackElements(basicDictionary: [:], onSuccess: { (feedbackItem) in
//
//        }) { (error) in
//            self.stopLoadingWithError(error: error)
//        }
//
//    }
//
//
//    @IBAction func evaluateButtonIsPressed(_ sender: Any) {
//        print("Evaluate")
//
//
//    }
//
//    @IBAction func cancelButtonIsPressed(_ sender: Any) {
//        print("Cancel")
//    }
//
//
//}
//
