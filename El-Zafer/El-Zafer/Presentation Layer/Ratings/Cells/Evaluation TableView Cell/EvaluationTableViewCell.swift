//
//  EvaluationTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 6/24/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import Cosmos

protocol feedBackTableViewCellDelegate {
    func didfeedBackViewRated(tag: Int, rate: Float)
}

class EvaluationTableViewCell: UITableViewCell {
    
    var delegate: feedBackTableViewCellDelegate?
    var tagArray: [Double] = []
    
    var viewModelsfeedbackItems : [FeedbackItem] = []{
        didSet {
            getTagArray()
        }
    }
    
    func getTagArray() {
        for extraService in self.viewModelsfeedbackItems {
            self.tagArray.append(3.0)
        }
    }
    
    func didfeedBackViewRated(tag: Int, rate: Float) {
        if let delegateValue = delegate {
            delegateValue.didfeedBackViewRated(tag: tag, rate: rate)
        }
    }

    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ratingView.settings.fillMode = .full
        
        ratingView.didTouchCosmos = { rating in
            self.rate(cosmosViewIndex: self.ratingView.tag - 1)
        }
        
    }
    
    func rate(cosmosViewIndex: Int) {
        didfeedBackViewRated(tag: cosmosViewIndex, rate: Float(self.ratingView!.rating))
    }
    
}
