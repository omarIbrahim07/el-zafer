//
//  NotificationsViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {
    
    var notifications: [Notificationn] = []
    var window: UIWindow?


    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getNotifications()
        // Do any additional setup after loading the view.
    }
    

    func configureTableView() {
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getNotifications() {
        
        weak var weakSelf = self
        
        let parameters: [String : AnyObject] = [
            :
        ]
        
        startLoading()
        
        PackageAPIManager().getNotifications(basicDictionary: parameters, onSuccess: { (myNotifications) in
            
            self.stopLoadingWithSuccess()
            self.notifications = myNotifications
            
            self.tableView.reloadData()
            
        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
//            if self.myPackages.count == 0 {
//                self.tableView.isHidden = true
//                self.noPackagesLabel.isHidden = false
//            }
        }
        
        
    }


}

extension NotificationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count > 0 ? self.notifications.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as? NotificationTableViewCell {
            
            if let from = self.notifications[indexPath.row].from, let to = self.notifications[indexPath.row].to, let price = self.notifications[indexPath.row].price {
                cell.notificationLabel.text = "لقد تم تسعير الشُحنة المتوجهة من: \(from) إلى: \(to) بسعر قدره \(price) جنيه"
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let price = self.notifications[indexPath.row].price, let orderID = self.notifications[indexPath.row].orderId {
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            if let viewController = storyboard.instantiateViewController(withIdentifier: "PriceCalculatedViewController") as? PriceCalculatedViewController {
                viewController.packageId = Int(orderID)
                viewController.calculatedPrice = Float(price)
                appDelegate.window!.rootViewController = viewController
                appDelegate.window!.makeKeyAndVisible()
            }
        }
    }
    
}
