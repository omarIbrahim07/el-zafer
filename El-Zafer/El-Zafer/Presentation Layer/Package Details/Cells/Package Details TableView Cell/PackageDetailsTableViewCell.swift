//
//  PackageDetailsTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class PackageDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var packageDetailsTitleLabel: UILabel!
    @IBOutlet weak var packageDetailsDescriptionLabel: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
}
