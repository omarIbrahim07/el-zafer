//
//  PackageDetailsViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class PackageDetailsViewController: BaseViewController {
    
    var packageId: Int?
    var package: Package?
    
    var cellTitles: [String] = [ "مواصفات الشُحنة" , "تاريخ طلب الشحنة" , "حالة الشُحنة" , "رقم الشُحنة"  ]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getPackage()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "PackageDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "PackageDetailsTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getPackage() {
        
        guard let packageId = self.packageId, packageId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الشُحنة"
            showError(error: apiError)
            return
        }
        
        let parameters: [String : AnyObject] = [
            "order_id" : self.packageId as AnyObject
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().getPackage(basicDictionary: parameters, onSuccess: { (package) in
            
            self.stopLoadingWithSuccess()
            self.package = package
            self.tableView.reloadData()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }

}

extension PackageDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {   
        
        if let cell: PackageDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PackageDetailsTableViewCell") as? PackageDetailsTableViewCell {
            
            cell.packageDetailsTitleLabel.text = cellTitles[indexPath.row]
            
            if indexPath.row == 0 {
                if let height = package?.height, let length = package?.length, let type = package?.packageType, let fromLocation = package?.fromLocation, let toLocation = package?.toLocation, let weight = package?.weight, let dimensionUnit = package?.dimensionUnit, let weightUnit = package?.weightUnit {
                    
                    let unitWeight = weightUnit == "1" ? "كجم" : "لتر"
                    let unitDimension = dimensionUnit == "1" ? "متر" : "بوصة"
                    
                    cell.packageDetailsDescriptionLabel.text = "\(type) وزن \(weight) \(unitWeight) مقاس \(length) * \(height) \(unitDimension) من عنوان : \(fromLocation) إلى : \(toLocation)"
                }
            } else if indexPath.row == 1 {
                cell.packageDetailsDescriptionLabel.text = self.package?.createdAt
            } else if indexPath.row == 2 {
                cell.packageDetailsDescriptionLabel.text = self.package?.orderStatus
            } else if indexPath.row == 3 {
                if let id = self.package?.id {
                    cell.packageDetailsDescriptionLabel.text = "\(id)"
                }
                cell.view.isHidden = true
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    
}
