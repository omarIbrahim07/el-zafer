//
//  SignUpViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/15/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var firstPhoneNumberTextField: UITextField!
    @IBOutlet weak var secondPhoneNumberTextField: UITextField!
    @IBOutlet weak var thirdPhoneNumberTextField: UITextField!
    @IBOutlet weak var landPhoneNumberTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var registrationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "تسجيل جديد"
        configureView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        userNameTextField.endEditing(true)
        emailAddressTextField.endEditing(true)
        firstPhoneNumberTextField.endEditing(true)
        secondPhoneNumberTextField.endEditing(true)
        thirdPhoneNumberTextField.endEditing(true)
        landPhoneNumberTextField.endEditing(true)
        passwordTextField.endEditing(true)
        confirmPasswordTextField.endEditing(true)
    }
    
    func configureView() {
        registrationView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        firstPhoneNumberTextField.delegate = self
        secondPhoneNumberTextField.delegate = self
        thirdPhoneNumberTextField.delegate = self
        landPhoneNumberTextField.delegate = self
    }
    
    func registerUser() {
        
        guard let username = userNameTextField.text, username.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال اسم المستخدم"
            showError(error: apiError)
            return
        }
        
        guard let email = emailAddressTextField.text, email.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let firstMobileNumber = firstPhoneNumberTextField.text, firstMobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let secondMobileNumber = secondPhoneNumberTextField.text, secondMobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let thirdMobileNumber = thirdPhoneNumberTextField.text, thirdMobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let landNumber = landPhoneNumberTextField.text, landNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let confirmPassword = confirmPasswordTextField.text, confirmPassword.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard password == confirmPassword else {
            let apiError = APIError()
            apiError.message = "كلمة المرور غير متطابقة"
            showError(error: apiError)
            return
        }
        
        let test = "test"
        let role_Id = 4
        
        let parameters = [
            "first_name" : username as AnyObject,
            "last_name" : test as AnyObject,
            "phone" : landNumber as AnyObject,
            "mobile1" : firstMobileNumber as AnyObject,
            "mobile2" : secondMobileNumber as AnyObject,
            "mobile3" : thirdMobileNumber as AnyObject,
            "email" : email as AnyObject,
            "password" : password as AnyObject,
            "role_id" : role_Id as AnyObject,
            "fcm_token" : FirebaseToken as AnyObject,
        ]
        
        weak var weakSelf = self
        
        startLoading()

        AuthenticationAPIManager().registerUser(basicDictionary: parameters, onSuccess: { (_) in

        weakSelf?.getUserProfile()

        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    //MARK:- Navigation
    func presentHomeScreen(isPushNotification: Bool = false) {
        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
            appDelegate.window!.rootViewController = viewController
            appDelegate.window!.makeKeyAndVisible()
        }
    }

    @IBAction func resgistrationButtonIsPressed(_ sender: Any) {
        registerUser()
        print("Registration")
    }
    
}

extension SignUpViewController: UITextFieldDelegate {
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//
//        print("70ssa")
//
//        let allowedCharacters = CharacterSet.decimalDigits
//        let characterSet = CharacterSet(charactersIn: string)
//        return allowedCharacters.isSuperset(of: characterSet)
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        print("70ssa")
        
        //        let allowedCharacters = CharacterSet.decimalDigits
        let allowedCharacters = CharacterSet(charactersIn:"0123456789١٢٣٤٥٦٧٨٩٠+")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
            UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}


