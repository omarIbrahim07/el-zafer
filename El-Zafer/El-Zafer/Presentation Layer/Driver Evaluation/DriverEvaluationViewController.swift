//
//  DriverEvaluationViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/13/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import Cosmos

class DriverEvaluationViewController: BaseViewController {
    
    var rate: Double?
    var comment: String?
    
    var myPackages: [Package] = []
    var idSelected: Int?

    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var evaluationButton: UIButton!
    @IBOutlet weak var packageIdTextField: UITextField!
    @IBOutlet weak var ratingCosmosView: CosmosView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        getMyPackages()
        rateDriverView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        packageIdTextField.endEditing(true)
        commentTextView.endEditing(true)        
    }
    
    func configureView() {
        evaluationButton.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        commentTextView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        createPicker(textField: packageIdTextField, tag: 0)
    }
    
    func rateDriverView() {
            ratingCosmosView.didTouchCosmos = { rating in
            self.rate = self.ratingCosmosView.rating
            print(self.rate)
        }
    }
    
    func getMyPackages() {
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().getMyPackages(basicDictionary: [:], onSuccess: { (packages) in
            
            self.stopLoadingWithSuccess()
            self.myPackages = packages
            print(self.myPackages)
            
        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
            self.showNoPackagePopUp()
        }
    }
    
    func createPicker(textField: UITextField,tag: Int) {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        textField.inputView = Picker
        Picker.tag = tag
    }
    
    func evaluateDriver() {
        
        guard let orderId = self.packageIdTextField.text, orderId.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الرحلة"
            showError(error: apiError)
            return
        }
        
        guard let rate = self.rate, rate >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال تقييم المندوب"
            showError(error: apiError)
            return
        }
        
        guard let comment = self.commentTextView.text, comment.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الرحلة"
            showError(error: apiError)
            return
        }
        
        print(orderId)
        print(rate)
        print(comment)
        
        let parameters: [String : AnyObject] = [
            "order_id" : orderId as AnyObject,
            "rating" : rate as AnyObject,
            "comment" : comment as AnyObject,
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().rateDriver(basicDictionary: parameters, onSuccess: { (_) in
            
        self.stopLoadingWithSuccess()
            print("7obby")
            
            self.showDonePopUp()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "لقد تم التقييم!", message: "شكرًا لتقييمك", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showNoPackagePopUp() {
        let alertController = UIAlertController(title: "عُذرًا لا يوجد لديك شُحنات !", message: "يمكنك التقييم بعد طلب شُحنة", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "إغلاق", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    @IBAction func evaluateButtonIsPressed(_ sender: Any) {
        print("Evaluate")
        evaluateDriver()
    }
    
    
}

extension DriverEvaluationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return myPackages.count > 0 ? 1 : 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return myPackages.count > 0 ? myPackages.count : 0
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            packageIdTextField.text = "\(myPackages[0].id ?? 0)"
            idSelected = myPackages[0].id
            return "\(myPackages[row].id ?? 0)"
        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            packageIdTextField.text = "\(myPackages[row].id ?? 0)"
            idSelected = myPackages[row].id
        }
        //            self.view.endEditing(true)
    }
    
}

extension DriverEvaluationViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension DriverEvaluationViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
