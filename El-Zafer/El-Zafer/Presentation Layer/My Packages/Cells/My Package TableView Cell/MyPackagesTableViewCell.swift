//
//  MyPackagesTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

protocol showPackageTableViewCellDelegate {
    func didshowPackageButtonPressed(tag: Int)
}

class MyPackagesTableViewCell: UITableViewCell {

    var showPackageDelegate: showPackageTableViewCellDelegate?
    
    var tagArray: [Bool] = []
    
//    var viewModelsPackages : [AdditionalService] = []{
//        didSet {
//            getTagArray()
//        }
//    }
    
//    func getTagArray() {
//
//        for extraService in self.viewModelsExtraServices {
//            self.tagArray.append(false)
//        }
//        print(tagArray)
//    }
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var myPackageLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var showPackageButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        // Initialization code
    }

    func configureView() {
        cellView.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        view.addCornerRadius(raduis: 5, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
    }
    
    //MARK:- Delegate Helpers
    func didshowPackageButtonPressed(tag: Int) {
        if let delegateValue = showPackageDelegate {
            delegateValue.didshowPackageButtonPressed(tag: tag)
        }
    }
    
    @IBAction func showPackageButtonIsPressed(_ sender: Any) {
        didshowPackageButtonPressed(tag: showPackageButton.tag)
        print("Show Button")
    }
}
