//
//  MyPackagesViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class MyPackagesViewController: BaseViewController {

    var myPackages: [Package] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noPackagesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getMyPackages()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "MyPackagesTableViewCell", bundle: nil), forCellReuseIdentifier: "MyPackagesTableViewCell")
        self.noPackagesLabel.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    func getMyPackages() {

        let parameters: [String : AnyObject] = [
            :
//            "order_id" : self.packageId as AnyObject
        ]
    
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().getMyPackages(basicDictionary: parameters, onSuccess: { (packages) in
            
            self.stopLoadingWithSuccess()
            self.myPackages = packages
            
            self.tableView.reloadData()
            
        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
            if self.myPackages.count == 0 {
                self.tableView.isHidden = true
                self.noPackagesLabel.isHidden = false
            }
        }
        
        
    }

    func goToTest() {
        if let myPackagesNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "PackageDetailsNavigationViewController") as? UINavigationController, let rootViewContoller = myPackagesNavigationController.viewControllers[0] as? PackageDetailsViewController {
            //                rootViewContoller.test = "test String"
            self.present(myPackagesNavigationController, animated: true, completion: nil)
            print("Fes")
        }
    }

}

extension MyPackagesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myPackages.count > 0 ? myPackages.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let myPackage = self.myPackages[indexPath.row]
        
        if let cell: MyPackagesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyPackagesTableViewCell") as? MyPackagesTableViewCell {
            
            print(myPackage.toLocation)
            print(myPackage.fromLocation)
            /* Printing string without optional */
            if let height = myPackage.height, let length = myPackage.length, let type = myPackage.packageType, let fromLocation = myPackage.fromLocation, let toLocation = myPackage.toLocation, let weight = myPackage.weight, let dimensionUnit = myPackage.dimensionUnit, let weightUnit = myPackage.weightUnit {
                
                let unitWeight = weightUnit == "1" ? "كجم" : "لتر"
                let unitDimension = dimensionUnit == "1" ? "متر" : "بوصة"
                
//                cell.myPackageLabel.text = "\(type) مقاس \(length) * \(height) من \(fromLocation) إلى \(toLocation)"
                cell.myPackageLabel.text = "\(type) وزن \(weight) \(unitWeight) مقاس \(length) * \(height) \(unitDimension) من العنوان: \(fromLocation) إلى العنوان: \(toLocation)"
            }
            
            cell.createdAtLabel.text = myPackage.createdAt
            cell.showPackageDelegate = self
            cell.showPackageButton.tag = indexPath.row
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension MyPackagesViewController: showPackageTableViewCellDelegate {
    
    func didshowPackageButtonPressed(tag: Int) {
        if let viewController = storyboard?.instantiateViewController(withIdentifier: "PackageDetailsViewController") as? PackageDetailsViewController {
            viewController.packageId = self.myPackages[tag].id
//            viewController.advertisementDetails = viewModel
//            viewController.advertisementID = "\(viewModel.id!)"
            self.navigationController?.show(viewController, sender: self)
        }
    }
    
}
