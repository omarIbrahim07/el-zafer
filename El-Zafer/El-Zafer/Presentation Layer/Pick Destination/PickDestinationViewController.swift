////
////  PickDestinationViewController.swift
////  El-Zafer
////
////  Created by Omar Ibrahim on 6/23/19.
////  Copyright © 2019 Egy designer. All rights reserved.
////
//
//import UIKit
//import MapKit
//
//class PickDestinationViewController: BaseViewController {
//
//    var shippingLocations: [ShippingLocation] = []
//
//    var controllerName: String?
//
//    var typeIdSelected: Int?
//    var weightUnitIdSelected: Int?
//    var lengthUnitIdSelected: Int?
//    var weight: Float?
//    var height: Float?
//    var width: Float?
//    var longitude: Float?
//    var breakableId: Int?
//    var encloseId: Int?
//    var storeId: Int?
//    var deliveryMethodId: Int?
//
//    var packageID: Int?
//    var choosed: Int?
//
//    let locationManager = CLLocationManager()
//
//    var previousLocation: CLLocation?
//
//    var fromDestinationLongitude: String?
//    var fromDestinationLatitude: String?
//    var toDestinationLongitude: String?
//    var toDestinationLatitude: String?
//
//    var fromLocation: String?
//    var toLocation: String?
//
//    var textFieldChoosed: String = ""
//    var pickButtonPressedChoosed: String = ""
//
//    var currentLongitude: String?
//    var currentLatitude: String?
//    var Selected: String?
//    var pressed: Int? = 0
//
//    let option = [
//        "نعم",
//        "لا"
//    ]
//
//    @IBOutlet weak var mapView: MKMapView!
//    @IBOutlet weak var fromTextField: UITextField!
//    @IBOutlet weak var toTextField: UITextField!
//    @IBOutlet weak var addressLabel: UILabel!
//    @IBOutlet weak var confirmPackageButton: UIButton!
//    @IBOutlet weak var fromPickDestinationButton: UIButton!
//    @IBOutlet weak var toPickDestinationButton: UIButton!
//    @IBOutlet weak var middleNavigatorImage: UIImageView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        configureView()
//        navigationItem.title = self.controllerName
//        getShippingLocations()
//
//        checkLocationServices()
//        //        mapSearchBar.delegate = self
//        fromTextField.delegate = self
//        toTextField.delegate = self
//
//        if self.choosed == 1 {
//            fromPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
//            toPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
//            fromTextField.addTarget(self, action: #selector(fromtextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
//            toTextField.addTarget(self, action: #selector(totextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
//        } else if choosed == 2 {
//            fromPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
//            toPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
//            fromTextField.addTarget(self, action: #selector(fromtextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
//            createPicker(textField: toTextField, tag: 1)
//        } else if choosed == 3 {
//            fromPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
//            toPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
//            createPicker(textField: fromTextField, tag: 0)
//            toTextField.addTarget(self, action: #selector(totextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
//        } else if choosed == 4 {
//            fromPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
//            toPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
//            mapView.isHidden = true
//            addressLabel.isHidden = true
//            middleNavigatorImage.isHidden = true
//            createPicker(textField: toTextField, tag: 1)
//            createPicker(textField: fromTextField, tag: 0)
//        }
//        // Do any additional setup after loading the view.
//    }
//
//    func configureView() {
//        toPickDestinationButton.imageView?.contentMode = .scaleAspectFit
//        fromPickDestinationButton.imageView?.contentMode = .scaleAspectFit
//        confirmPackageButton.imageView?.contentMode = .scaleAspectFit
//    }
//
//    func getShippingLocations() {
//
//        weak var weakSelf = self
//
//        startLoading()
//
//        PackageAPIManager().getShippingLocations(basicDictionary: [:], onSuccess: { (shippingLocations) in
//
//            self.shippingLocations = shippingLocations
//            print(self.shippingLocations)
//
//            self.stopLoadingWithSuccess()
//
//        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
//        }
//
//    }
//
//    func createPicker(textField: UITextField, tag: Int) {
//
//        let Picker = UIPickerView()
//        Picker.delegate = self as! UIPickerViewDelegate
//        Picker.tag = tag
//
//        textField.inputView = Picker
//    }
//
//
//    @IBAction func nextIsPressed(_ sender: Any) {
//
//        setLocation()
//
////        print("Pressed")
////
////        if pressed == 0 {
////            sendPackage()
////            pressed = 1
////            confirmPackageButton.titleLabel?.text = "أكّد الموقع"
////        } else if pressed == 1 {
////            setLocation()
////            pressed = 0
////            confirmPackageButton.titleLabel?.text = "أكّد الشُحنة"
////        }
//
//    }
//
//    func sendPackage() {
//
//        guard let longitude = self.longitude, longitude > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال اسم المستخدم"
//            showError(error: apiError)
//            return
//        }
//
//        guard let width = self.width, width > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال البريد الالكتروني"
//            showError(error: apiError)
//            return
//        }
//
//        guard let height = self.height, height > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
//
//        guard let heightDimension = self.lengthUnitIdSelected, heightDimension > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
//
//        guard let weight = self.weight, weight > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
//
//        guard let weightUnit = self.weightUnitIdSelected, weightUnit > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
//
//        guard let type = self.typeIdSelected, type > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال كلمة المرور"
//            showError(error: apiError)
//            return
//        }
//
//        guard let deliveryMethod = self.deliveryMethodId, deliveryMethod > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
//            showError(error: apiError)
//            return
//        }
//
//        guard let breakable = self.breakableId, breakable >= 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
//            showError(error: apiError)
//            return
//        }
//
//        guard let packaging = self.encloseId, packaging >= 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
//            showError(error: apiError)
//            return
//        }
//
//        guard let storing = self.storeId, storing >= 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال تأكيد كلمة المرور المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let fromLongitude = self.fromDestinationLongitude, let fromLatitude = self.fromDestinationLatitude, fromLongitude.count > 0,fromLatitude.count > 0  else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let toLongitude = self.toDestinationLongitude, let toLatitude = self.toDestinationLatitude, toLongitude.count > 0, toLatitude.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let toTextField = self.toTextField.text, toTextField.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let fromTextField = self.fromTextField.text, fromTextField.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        let orderStatus = "1"
//
//        let parameters = [
//            "length" : self.height as AnyObject,
//            "width" : self.width as AnyObject,
//            "height" : self.longitude as AnyObject,
//            "dim_unit" : self.lengthUnitIdSelected as AnyObject,
//            "weight" : self.weight as AnyObject,
//            "weight_unit" : self.weightUnitIdSelected as AnyObject,
//            "category_id" : self.typeIdSelected as AnyObject,
//            "orderStatus_id" : orderStatus as AnyObject,
//            "deliveryMethod_id" : self.deliveryMethodId as AnyObject,
//            "breakable" : self.breakableId as AnyObject,
//            "packaging" : self.encloseId as AnyObject,
//            "storage" : self.storeId as AnyObject
//        ]
//
//        weak var weakSelf = self
//
//        startLoading()
//
//        PackageAPIManager().addPackage(basicDictionary: parameters, onSuccess: { (packageIdd) in
//
//            self.stopLoadingWithSuccess()
//            print(packageIdd)
//
//            self.packageID = packageIdd
//
//        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
//        }
//    }
//
//    func setLocation() {
//
//        self.fromLocation = fromTextField.text
//        self.toLocation = toTextField.text
//
//        guard let myPackageId = self.packageID, myPackageId > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال رقم الشُحنة"
//            showError(error: apiError)
//            return
//        }
//
//        guard let fromLongitude = self.fromDestinationLongitude, let fromLatitude = self.fromDestinationLatitude, fromLongitude.count > 0,fromLatitude.count > 0  else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let toLongitude = self.toDestinationLongitude, let toLatitude = self.toDestinationLatitude, toLongitude.count > 0, toLatitude.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let toTextField = self.toLocation, toTextField.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard let fromTextField = self.fromLocation, fromTextField.count > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
//            showError(error: apiError)
//            return
//        }
//
//        guard toTextField != fromTextField else {
//            let apiError = APIError()
//            apiError.message = "من فضلك ادخل العنوان بشكل صحيح"
//            showError(error: apiError)
//            return
//        }
//
//        weak var weakSelf = self
//
//        let params = [
//            "order_id" : self.packageID as AnyObject,
//            "from_long" : self.fromDestinationLongitude as AnyObject,
//            "from_lat" : self.fromDestinationLatitude as AnyObject,
//            "to_long" : self.toDestinationLongitude as AnyObject,
//            "to_lat" : self.toDestinationLatitude as AnyObject,
//            "loc_to" : self.toLocation as AnyObject,
//            "loc_from" : self.fromLocation as AnyObject
//            ]
//
//        startLoading()
//        PackageAPIManager().addPackageLocation(basicDictionary: params, onSuccess: { (saved) in
//
//            self.stopLoadingWithSuccess()
//            self.goToExtraServices(packageId: self.packageID!)
//
//        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
//        }
//
//    }
//
//
//    func whenPickPressed(pickButtonChoosed: String, textField: UITextField) {
//
//        pickButtonPressedChoosed = pickButtonChoosed
//
//        let geoCoder = CLGeocoder()
//        let center = getCenterLocation(for: mapView)
//
//        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
//
//            guard let self = self else {
//                return
//            }
//
//            if let _ = error {
//
//            }
//
//            guard let placemark = placemarks?.first else {
//                return
//            }
//
//            let streetNumber = placemark.subThoroughfare ?? ""
//            let streetName = placemark.thoroughfare ?? ""
//
//            DispatchQueue.main.async {
//                self.addressLabel.text = "\(streetNumber) \(streetName)"
//            }
//
//        }
//        print("Yes")
//        textField.text = self.addressLabel.text
//
//    }
//
//    func whenTextFieldIsPressed(textField: UITextField, choosedTextField: String) {
//
//        let geoCoder = CLGeocoder()
//        geoCoder.geocodeAddressString(textField.text!) { (placemarks:[CLPlacemark]?, error:Error?) in
//            if error == nil {
//                self.textFieldChoosed = choosedTextField
//
//                let placemark = placemarks?.first
//
//                let annotation = MKPointAnnotation()
//                annotation.coordinate = (placemark?.location?.coordinate)!
//
//                let span = MKCoordinateSpan(latitudeDelta: 0.075, longitudeDelta: 0.075)
//                let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
//
//                self.mapView.setRegion(region, animated: true)
//
////                annotation.title = self.mapSearchBar.text
//
//                self.mapView.addAnnotation(annotation)
//                self.mapView.selectAnnotation(annotation, animated: true)
//                print("Yes")
//
//                self.textFieldChoosed = ""
//
//
//            } else {
//                print(error)
//            }
//        }
//
//    }
//
//    @IBAction func pickToPressed(_ sender: Any) {
//        if choosed == 1 {
//            whenPickPressed(pickButtonChoosed: "To", textField: toTextField)
//        } else if choosed == 3 {
//            whenPickPressed(pickButtonChoosed: "To", textField: toTextField)
//        }
//    }
//
//
//    @IBAction func pickFromPressed(_ sender: Any) {
//        if choosed == 1 {
//            whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
//        } else if choosed == 2 {
//            whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
//        }
//    }
//
//
//    @objc func fromtextFieldDidEnd(_ textField: UITextField) {
//        whenTextFieldIsPressed(textField: fromTextField, choosedTextField: "From")
//    }
//
//    @objc func totextFieldDidEndOnExit(_ textField: UITextField) {
//        whenTextFieldIsPressed(textField: fromTextField, choosedTextField: "To")
//    }
//
//    @objc func fromtextFieldDidEndOnExit(_ textField: UITextField) {
//        whenTextFieldIsPressed(textField: fromTextField, choosedTextField: "From")
//    }
//
//    func showLocationDisabledPopUp() {
//        let alertController = UIAlertController(title: "خاصية تحديد المواقع غير مفعلة", message: "من فضلك اذهب إلى الإعدادات لتفعيل خاصية تحديد المواقع", preferredStyle: .alert)
//        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        let openAction = UIAlertAction(title: "إذهب إلى الإعدادات", style: .default) { (action) in
//            if let url = URL(string: UIApplication.openSettingsURLString) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            }
//        }
//        alertController.addAction(openAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
//
//    func checkLocationAuthorization() {
//        switch CLLocationManager.authorizationStatus() {
//        case .authorizedWhenInUse:
//            // Do map stuff
//            startTrackingUserLocztion()
//        //            break
//        case .denied:
//            // Show alert instructing them to turn to permissions
//            showLocationDisabledPopUp()
//            break
//        case .notDetermined:
//            //            checkLocationServices()
//            print("not determined")
//            locationManager.requestWhenInUseAuthorization()
//            break
//        case .restricted:
//            break
//        case .authorizedAlways:
//            break
//        default:
//            break
//        }
//    }
//
//    func centerViewOnUserLocation() {
//        if let location = locationManager.location?.coordinate {
//            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: 100, longitudinalMeters: 100)
//            mapView.setRegion(region, animated: true)
//        }
//    }
//
//    func startTrackingUserLocztion() {
//        mapView.showsUserLocation = true
//        centerViewOnUserLocation()
//        locationManager.startUpdatingLocation()
//        previousLocation = getCenterLocation(for: mapView)
//        mapView.showsScale = true
//        mapView.showsTraffic = true
//
//        print("7obby")
//    }
//
//    func setupLocationManager() {
//        mapView.delegate = self
//        locationManager.delegate = self
//        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//    }
//
//    func checkLocationServices() {
//        if CLLocationManager.locationServicesEnabled() {
//            // setup our location manager
//            setupLocationManager()
//            checkLocationAuthorization()
//        } else {
//            print("مجتش")
//            // show alert letting the user know they have to turn this on
//            showLocationDisabledPopUp()
//        }
//    }
//
//    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
//
//        print("SSSSSS")
//
//        let latitude = mapView.centerCoordinate.latitude
//        let longitude = mapView.centerCoordinate.longitude
//
//        if textFieldChoosed == "To" {
//            toDestinationLatitude = String(latitude)
//            toDestinationLongitude = String(longitude)
//
//            print("ToLongitude: \(toDestinationLongitude)")
//            print("ToLatitude: \(toDestinationLatitude)")
//
//            self.textFieldChoosed = ""
//        } else if textFieldChoosed == "From" {
//
//            fromDestinationLatitude = String(latitude)
//            fromDestinationLongitude = String(longitude)
//
//            print("FromLongitude: \(fromDestinationLongitude)")
//            print("FromLatitude: \(fromDestinationLatitude)")
//
//            self.textFieldChoosed = ""
//        } else if pickButtonPressedChoosed == "From" {
//            fromDestinationLatitude = String(latitude)
//            fromDestinationLongitude = String(longitude)
//
//            print("FromLongitude: \(fromDestinationLongitude)")
//            print("FromLatitude: \(fromDestinationLatitude)")
//
//            self.pickButtonPressedChoosed = ""
//        } else if pickButtonPressedChoosed == "To" {
//            toDestinationLatitude = String(latitude)
//            toDestinationLongitude = String(longitude)
//
//            print("ToLongitude: \(toDestinationLongitude)")
//            print("ToLatitude: \(toDestinationLatitude)")
//
//            self.pickButtonPressedChoosed = ""
//        } else if self.textFieldChoosed == "DeFrom" {
//            fromDestinationLatitude = String(0)
//            fromDestinationLongitude = String(0)
//
//            print("FromLongitude: \(fromDestinationLongitude)")
//            print("FromLatitude: \(fromDestinationLatitude)")
//
//            self.textFieldChoosed = ""
//        }
//
//        return CLLocation(latitude: latitude, longitude: longitude)
//    }
//
//    func goToExtraServices(packageId: Int) {
//        print("PackageId: \(packageId)")
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "ExtraServicesViewController") as! ExtraServicesViewController
//        viewController.postId = packageId
//        viewController.storable = self.storeId
//        navigationController?.pushViewController(viewController, animated: true)
//    }
//
//
//
//}
//
//extension PickDestinationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        if pickerView.tag == 0 {
//            return self.shippingLocations.count
//        } else if pickerView.tag == 1 {
//            return self.shippingLocations.count
//        }
//        return 0
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        if pickerView.tag == 0 {
//            fromTextField.text = self.shippingLocations[0].name
//            Selected = self.shippingLocations[0].name
//            fromDestinationLatitude = self.shippingLocations[0].latitude
//            fromDestinationLongitude = self.shippingLocations[0].longitude
//            return self.shippingLocations[row].name
//        } else if pickerView.tag == 1 {
//            toTextField.text = self.shippingLocations[0].name
//            Selected = self.shippingLocations[0].name
//            toDestinationLatitude = self.shippingLocations[0].latitude
//            toDestinationLongitude = self.shippingLocations[0].longitude
//            return self.shippingLocations[row].name
//        }
//        return ""
//    }
//
//
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        if pickerView.tag == 0 {
//
//            Selected = self.shippingLocations[row].name
//            fromTextField.text = Selected
//            fromDestinationLatitude = self.shippingLocations[row].latitude
//            fromDestinationLongitude = self.shippingLocations[row].longitude
//            print("fromDestinationLongitude: \(fromDestinationLongitude)")
//            print("fromDestinationLatitude: \(fromDestinationLatitude)")
////            self.view.endEditing(true)
//        } else if pickerView.tag == 1 {
//
//            Selected = self.shippingLocations[row].name
//            toTextField.text = Selected
//            toDestinationLatitude = self.shippingLocations[row].latitude
//            toDestinationLongitude = self.shippingLocations[row].longitude
//            print("toDestinationLongitude: \(toDestinationLongitude)")
//            print("toDestinationLatitude: \(toDestinationLatitude)")
//
//        }
//    }
//
//
//
//
//}
//
//
//extension PickDestinationViewController: UITextFieldDelegate {
//
//}
//
//extension PickDestinationViewController: CLLocationManagerDelegate {
//
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        checkLocationAuthorization()
//    }
//
//}
//
//extension PickDestinationViewController: MKMapViewDelegate {
//
//
//
//    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//
//        print("دخلت")
//
//        let center = getCenterLocation(for: mapView)
//        let geoCoder = CLGeocoder()
//
//        guard let previousLocation = self.previousLocation else {
//            return
//        }
//
//        guard center.distance(from: previousLocation) > 2 else {
//            return
//        }
//
//        self.previousLocation = center
//
//        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
//
//            guard let self = self else {
//                return
//            }
//
//            if let _ = error {
//
//            }
//
//            guard let placemark = placemarks?.first else {
//                return
//            }
//
//            let streetNumber = placemark.subThoroughfare ?? ""
//            let streetName = placemark.thoroughfare ?? ""
//
//            DispatchQueue.main.async {
//                self.addressLabel.text = "\(streetNumber) \(streetName)"
//            }
//
//        }
//    }
//
//}

//
//  PickDestinationViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 6/23/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import MapKit

class PickDestinationViewController: BaseViewController {
    
    var shippingLocations: [ShippingLocation] = []
    
    var controllerName: String?
    
    var typeIdSelected: Int?
    var weightUnitIdSelected: Int?
    var lengthUnitIdSelected: Int?
    var weight: Float?
    var height: Float?
    var width: Float?
    var longitude: Float?
    var breakableId: Int?
    var encloseId: Int?
    var storeId: Int?
    var deliveryMethodId: Int?
    
    var packageID: Int?
    var choosed: Int?
    
    let locationManager = CLLocationManager()
    
    var previousLocation: CLLocation?
    
    var fromDestinationLongitude: String?
    var fromDestinationLatitude: String?
    var toDestinationLongitude: String?
    var toDestinationLatitude: String?
    
    var fromLocation: String?
    var toLocation: String?
    
    var textFieldChoosed: String = ""
    var pickButtonPressedChoosed: String = ""
    
    var currentLongitude: String?
    var currentLatitude: String?
    var Selected: String?
    var pressed: Int? = 0
    
    let option = [
        "نعم",
        "لا"
    ]
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var addressLabel: UILabel!
//    @IBOutlet weak var confirmPackageButton: UIButton!
    @IBOutlet weak var fromPickDestinationButton: UIButton!
    @IBOutlet weak var toPickDestinationButton: UIButton!
    @IBOutlet weak var middleNavigatorImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        configureNavigationBar()
        navigationItem.title = self.controllerName
        getShippingLocations()
        
        checkLocationServices()
        //        mapSearchBar.delegate = self
//        fromTextField.delegate = self
//        toTextField.delegate = self
        
        if self.choosed == 1 {
            fromTextField.delegate = self
            toTextField.delegate = self
            fromPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
            toPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
            fromTextField.addTarget(self, action: #selector(fromtextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
            toTextField.addTarget(self, action: #selector(totextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
        } else if choosed == 2 {
            fromTextField.delegate = self
            fromPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
            toPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
            fromTextField.addTarget(self, action: #selector(fromtextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
            createPicker(textField: toTextField, tag: 1)
        } else if choosed == 3 {
            toTextField.delegate = self
            fromPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
            toPickDestinationButton.setImage(UIImage(named: "placeholder"), for: .normal)
            createPicker(textField: fromTextField, tag: 0)
            toTextField.addTarget(self, action: #selector(totextFieldDidEndOnExit(_:)), for: .editingDidEndOnExit)
        } else if choosed == 4 {
            fromPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
            toPickDestinationButton.setImage(UIImage(named: "placeholder to"), for: .normal)
            mapView.isHidden = true
            addressLabel.isHidden = true
            middleNavigatorImage.isHidden = true
            createPicker(textField: toTextField, tag: 1)
            createPicker(textField: fromTextField, tag: 0)
        }
        // Do any additional setup after loading the view.
    }
    
    func configureView() {
        toPickDestinationButton.imageView?.contentMode = .scaleAspectFit
        fromPickDestinationButton.imageView?.contentMode = .scaleAspectFit
//        confirmPackageButton.imageView?.contentMode = .scaleAspectFit
    }
    
    func configureNavigationBar() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "تأكيد العناوين", style: .plain, target: self, action: #selector(setYourLocation))
    }
    
    @objc fileprivate func setYourLocation() {
        setLocation()
    }
    
    func getShippingLocations() {
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().getShippingLocations(basicDictionary: [:], onSuccess: { (shippingLocations) in
            
            self.shippingLocations = shippingLocations
            print(self.shippingLocations)
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func createPicker(textField: UITextField, tag: Int) {
        
        let Picker = UIPickerView()
        Picker.delegate = self as! UIPickerViewDelegate
        Picker.tag = tag
        
        textField.inputView = Picker
    }
    
    
//    @IBAction func nextIsPressed(_ sender: Any) {
//        
//        setLocation()
//        
//    }
    
    func sendPackage() {
        
        guard let longitude = self.longitude, longitude > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال اسم المستخدم"
            showError(error: apiError)
            return
        }
        
        guard let width = self.width, width > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let height = self.height, height > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let heightDimension = self.lengthUnitIdSelected, heightDimension > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let weight = self.weight, weight > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let weightUnit = self.weightUnitIdSelected, weightUnit > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let type = self.typeIdSelected, type > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let deliveryMethod = self.deliveryMethodId, deliveryMethod > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let breakable = self.breakableId, breakable >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let packaging = self.encloseId, packaging >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let storing = self.storeId, storing >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور المُراد"
            showError(error: apiError)
            return
        }
        
        guard let fromLongitude = self.fromDestinationLongitude, let fromLatitude = self.fromDestinationLatitude, fromLongitude.count > 0,fromLatitude.count > 0  else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard let toLongitude = self.toDestinationLongitude, let toLatitude = self.toDestinationLatitude, toLongitude.count > 0, toLatitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard let toTextField = self.toTextField.text, toTextField.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard let fromTextField = self.fromTextField.text, fromTextField.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        let orderStatus = "1"
        
        let parameters = [
            "length" : self.height as AnyObject,
            "width" : self.width as AnyObject,
            "height" : self.longitude as AnyObject,
            "dim_unit" : self.lengthUnitIdSelected as AnyObject,
            "weight" : self.weight as AnyObject,
            "weight_unit" : self.weightUnitIdSelected as AnyObject,
            "category_id" : self.typeIdSelected as AnyObject,
            "orderStatus_id" : orderStatus as AnyObject,
            "deliveryMethod_id" : self.deliveryMethodId as AnyObject,
            "breakable" : self.breakableId as AnyObject,
            "packaging" : self.encloseId as AnyObject,
            "storage" : self.storeId as AnyObject
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().addPackage(basicDictionary: parameters, onSuccess: { (packageIdd) in
            
            self.stopLoadingWithSuccess()
            print(packageIdd)
            
            self.packageID = packageIdd
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func setLocation() {
        
        self.fromLocation = fromTextField.text
        self.toLocation = toTextField.text
        
        guard let myPackageId = self.packageID, myPackageId > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال رقم الشُحنة"
            showError(error: apiError)
            return
        }
        
        guard let fromLongitude = self.fromDestinationLongitude, let fromLatitude = self.fromDestinationLatitude, fromLongitude.count > 0,fromLatitude.count > 0  else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard let toLongitude = self.toDestinationLongitude, let toLatitude = self.toDestinationLatitude, toLongitude.count > 0, toLatitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard let toTextField = self.toLocation, toTextField.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان وصول الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard let fromTextField = self.fromLocation, fromTextField.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال عنوان إحضار الشحنة المُراد"
            showError(error: apiError)
            return
        }
        
        guard toTextField != fromTextField else {
            let apiError = APIError()
            apiError.message = "من فضلك ادخل العنوان بشكل صحيح"
            showError(error: apiError)
            return
        }
        
        weak var weakSelf = self
        
        let params = [
            "order_id" : self.packageID as AnyObject,
            "from_long" : self.fromDestinationLongitude as AnyObject,
            "from_lat" : self.fromDestinationLatitude as AnyObject,
            "to_long" : self.toDestinationLongitude as AnyObject,
            "to_lat" : self.toDestinationLatitude as AnyObject,
            "loc_to" : self.toLocation as AnyObject,
            "loc_from" : self.fromLocation as AnyObject
        ]
        
        startLoading()
        PackageAPIManager().addPackageLocation(basicDictionary: params, onSuccess: { (saved) in
            
            self.stopLoadingWithSuccess()
            self.goToExtraServices(packageId: self.packageID!)
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    
    func whenPickPressed(pickButtonChoosed: String, textField: UITextField) {
        
        pickButtonPressedChoosed = pickButtonChoosed
        
        let geoCoder = CLGeocoder()
        let center = getCenterLocation(for: mapView)
        
        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
            
            guard let self = self else {
                return
            }
            
            if let _ = error {
                
            }
            
            guard let placemark = placemarks?.first else {
                return
            }
            
            let streetNumber = placemark.subThoroughfare ?? ""
            let streetName = placemark.thoroughfare ?? ""
            
            DispatchQueue.main.async {
                self.addressLabel.text = "\(streetNumber) \(streetName)"
            }
            
        }
        print("Yes")
        textField.text = self.addressLabel.text
        
    }
    
    func whenTextFieldIsPressed(textField: UITextField, choosedTextField: String) {
        
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(textField.text!) { (placemarks:[CLPlacemark]?, error:Error?) in
            if error == nil {
                self.textFieldChoosed = choosedTextField
                
                let placemark = placemarks?.first
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = (placemark?.location?.coordinate)!
                
                let span = MKCoordinateSpan(latitudeDelta: 0.075, longitudeDelta: 0.075)
                let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
                
                self.mapView.setRegion(region, animated: true)
                
                //                annotation.title = self.mapSearchBar.text
                
                self.mapView.addAnnotation(annotation)
                self.mapView.selectAnnotation(annotation, animated: true)
                print("Yes")
                
                self.textFieldChoosed = ""
                
                
            } else {
                print(error)
            }
        }
        
    }
    
    @IBAction func pickToPressed(_ sender: Any) {
//        if choosed == 1 {
//            whenPickPressed(pickButtonChoosed: "To", textField: toTextField)
//        } else if choosed == 3 {
//            whenPickPressed(pickButtonChoosed: "To", textField: toTextField)
//        }
    }
    
    
    @IBAction func pickFromPressed(_ sender: Any) {
//        if choosed == 1 {
//            whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
//        } else if choosed == 2 {
//            whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
//        }
    }
    
    
    @objc func fromtextFieldDidEnd(_ textField: UITextField) {
        whenTextFieldIsPressed(textField: fromTextField, choosedTextField: "From")
        print("7ob")
    }
    
    @objc func totextFieldDidEndOnExit(_ textField: UITextField) {
        whenTextFieldIsPressed(textField: fromTextField, choosedTextField: "To")
    }
    
    @objc func fromtextFieldDidEndOnExit(_ textField: UITextField) {
        whenTextFieldIsPressed(textField: fromTextField, choosedTextField: "From")
    }
    
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "خاصية تحديد المواقع غير مفعلة", message: "من فضلك اذهب إلى الإعدادات لتفعيل خاصية تحديد المواقع", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "إذهب إلى الإعدادات", style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            // Do map stuff
            startTrackingUserLocztion()
        //            break
        case .denied:
            // Show alert instructing them to turn to permissions
            showLocationDisabledPopUp()
            break
        case .notDetermined:
            //            checkLocationServices()
            print("not determined")
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        case .authorizedAlways:
            break
        default:
            break
        }
    }
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: 100, longitudinalMeters: 100)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func startTrackingUserLocztion() {
        mapView.showsUserLocation = true
        centerViewOnUserLocation()
        locationManager.startUpdatingLocation()
        previousLocation = getCenterLocation(for: mapView)
        mapView.showsScale = true
        mapView.showsTraffic = true
        
        print("7obby")
    }
    
    func setupLocationManager() {
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            // setup our location manager
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            print("مجتش")
            // show alert letting the user know they have to turn this on
            showLocationDisabledPopUp()
        }
    }
    
    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        
        print("SSSSSS")
        
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        
        if textFieldChoosed == "To" {
            toDestinationLatitude = String(latitude)
            toDestinationLongitude = String(longitude)
            
            print("ToLongitude: \(toDestinationLongitude)")
            print("ToLatitude: \(toDestinationLatitude)")
            
            self.textFieldChoosed = ""
        } else if textFieldChoosed == "From" {
            
            fromDestinationLatitude = String(latitude)
            fromDestinationLongitude = String(longitude)
            
            print("FromLongitude: \(fromDestinationLongitude)")
            print("FromLatitude: \(fromDestinationLatitude)")
            
            self.textFieldChoosed = ""
        } else if pickButtonPressedChoosed == "From" {
            fromDestinationLatitude = String(latitude)
            fromDestinationLongitude = String(longitude)
            
            print("FromLongitude: \(fromDestinationLongitude)")
            print("FromLatitude: \(fromDestinationLatitude)")
            
            self.pickButtonPressedChoosed = ""
        } else if pickButtonPressedChoosed == "To" {
            toDestinationLatitude = String(latitude)
            toDestinationLongitude = String(longitude)
            
            print("ToLongitude: \(toDestinationLongitude)")
            print("ToLatitude: \(toDestinationLatitude)")
            
            self.pickButtonPressedChoosed = ""
        } else if self.textFieldChoosed == "DeFrom" {
            fromDestinationLatitude = String(0)
            fromDestinationLongitude = String(0)
            
            print("FromLongitude: \(fromDestinationLongitude)")
            print("FromLatitude: \(fromDestinationLatitude)")
            
            self.textFieldChoosed = ""
        }
        
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    func goToExtraServices(packageId: Int) {
        print("PackageId: \(packageId)")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ExtraServicesViewController") as! ExtraServicesViewController
        viewController.postId = packageId
        viewController.storable = self.storeId
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @IBAction func besmAllah(_ sender: Any) {
        print("Besm allah")
        locationManager.startUpdatingLocation()
    }
    
    
    
}

extension PickDestinationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return self.shippingLocations.count
        } else if pickerView.tag == 1 {
            return self.shippingLocations.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            fromTextField.text = self.shippingLocations[0].name
            Selected = self.shippingLocations[0].name
            fromDestinationLatitude = self.shippingLocations[0].latitude
            fromDestinationLongitude = self.shippingLocations[0].longitude
            return self.shippingLocations[row].name
        } else if pickerView.tag == 1 {
            toTextField.text = self.shippingLocations[0].name
            Selected = self.shippingLocations[0].name
            toDestinationLatitude = self.shippingLocations[0].latitude
            toDestinationLongitude = self.shippingLocations[0].longitude
            return self.shippingLocations[row].name
        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            
            Selected = self.shippingLocations[row].name
            fromTextField.text = Selected
            fromDestinationLatitude = self.shippingLocations[row].latitude
            fromDestinationLongitude = self.shippingLocations[row].longitude
            print("fromDestinationLongitude: \(fromDestinationLongitude)")
            print("fromDestinationLatitude: \(fromDestinationLatitude)")
            //            self.view.endEditing(true)
        } else if pickerView.tag == 1 {
            
            Selected = self.shippingLocations[row].name
            toTextField.text = Selected
            toDestinationLatitude = self.shippingLocations[row].latitude
            toDestinationLongitude = self.shippingLocations[row].longitude
            print("toDestinationLongitude: \(toDestinationLongitude)")
            print("toDestinationLatitude: \(toDestinationLatitude)")
            
        }
    }
    
    
    
    
}


extension PickDestinationViewController: UITextFieldDelegate {
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("2eshta")
        
        if textField.tag == 0 {
            if choosed == 1 {
                whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
            } else if choosed == 2 {
                whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
            }
        } else if textField.tag == 1 {
            if choosed == 1 {
                whenPickPressed(pickButtonChoosed: "To", textField: toTextField)
            } else if choosed == 3 {
                whenPickPressed(pickButtonChoosed: "To", textField: toTextField)
            }
        }
        
        return true
    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        print("2eshta")
//
//        if textField.tag == 0 {
//            if choosed == 1 {
//                whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
//            } else if choosed == 2 {
//                whenPickPressed(pickButtonChoosed: "From", textField: fromTextField)
//            }
//        }
//
//        return true
//    }
    
}

extension PickDestinationViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Access the last object from locations to get perfect current location
        if let location = locations.last {
            let span = MKCoordinateSpan(latitudeDelta: 0.00775, longitudeDelta: 0.00775)
            let myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
            let region = MKCoordinateRegion(center: myLocation, span: span)
            mapView.setRegion(region, animated: true)
        }
        self.mapView.showsUserLocation = true
        locationManager.stopUpdatingLocation()
    }
    
}

extension PickDestinationViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        print("دخلت")
        
        let center = getCenterLocation(for: mapView)
        let geoCoder = CLGeocoder()
        
        guard let previousLocation = self.previousLocation else {
            return
        }
        
        guard center.distance(from: previousLocation) > 2 else {
            return
        }
        
        self.previousLocation = center
        
        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
            
            guard let self = self else {
                return
            }
            
            if let _ = error {
                
            }
            
            guard let placemark = placemarks?.first else {
                return
            }
            
            let streetNumber = placemark.subThoroughfare ?? ""
            let streetName = placemark.thoroughfare ?? ""
            
            DispatchQueue.main.async {
                self.addressLabel.text = "\(streetNumber) \(streetName)"
            }
            
        }
    }
    
}

