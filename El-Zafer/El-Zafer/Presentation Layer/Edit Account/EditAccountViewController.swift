//
//  EditAccountViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import AssetsPickerViewController
import Toast_Swift
import PKHUD
import Photos

class EditAccountViewController: BaseViewController {
    
    var oldEmail = UserDefaultManager.shared.currentUser?.email
    var myImage: UIImage? = UIImage()
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var firstPhoneNumberTextField: UITextField!
    @IBOutlet weak var secondPhoneNumberTextField: UITextField!
    @IBOutlet weak var thirdPhoneNumberTextField: UITextField!
    @IBOutlet weak var landPhoneNumberTextField: UITextField!
    @IBOutlet weak var emailAddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmationView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        submitUserData()

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        userNameTextField.endEditing(true)
        emailAddressTextField.endEditing(true)
        firstPhoneNumberTextField.endEditing(true)
        secondPhoneNumberTextField.endEditing(true)
        thirdPhoneNumberTextField.endEditing(true)
        landPhoneNumberTextField.endEditing(true)
        passwordTextField.endEditing(true)

    }
    
    func configureView() {
        confirmationView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        changeTextFieldPlaceHolderColor(textField: userNameTextField, placeHolderString: "الإسم", fontSize: 15)
        changeTextFieldPlaceHolderColor(textField: firstPhoneNumberTextField, placeHolderString: "موبايل ١", fontSize: 15)
        changeTextFieldPlaceHolderColor(textField: secondPhoneNumberTextField, placeHolderString: "موبايل ٢", fontSize: 15)
        changeTextFieldPlaceHolderColor(textField: thirdPhoneNumberTextField, placeHolderString: "موبايل ٣", fontSize: 15)
        changeTextFieldPlaceHolderColor(textField: landPhoneNumberTextField, placeHolderString: "خط أرضي", fontSize: 15)
        changeTextFieldPlaceHolderColor(textField: emailAddressTextField, placeHolderString: "البريد الإلكتروني", fontSize: 15)
        changeTextFieldPlaceHolderColor(textField: passwordTextField, placeHolderString: "كلمة المرور", fontSize: 15)
        userImage.addCornerRadius(raduis: 15.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        firstPhoneNumberTextField.delegate = self
        secondPhoneNumberTextField.delegate = self
        thirdPhoneNumberTextField.delegate = self
        landPhoneNumberTextField.delegate = self

    }
    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        // for placeholder font color and font bold
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 0.3294117647, green: 0.6784313725, blue: 0.3529411765, alpha: 1), .font: UIFont.boldSystemFont(ofSize: fontSize)])
        
    }
    
    func submitUserData() {
//        userNameTextField.text = "\(UserDefaultManager.shared.currentUser?.firstName ?? "O") \(UserDefaultManager.shared.currentUser?.lastName ?? "M")"
        userNameTextField.text = UserDefaultManager.shared.currentUser?.firstName 
        firstPhoneNumberTextField.text = "\(UserDefaultManager.shared.currentUser?.mobile1 ?? "0")"
        secondPhoneNumberTextField.text = "\(UserDefaultManager.shared.currentUser?.mobile2 ?? "0")"
        thirdPhoneNumberTextField.text = "\(UserDefaultManager.shared.currentUser?.mobile3 ?? "0")"
        landPhoneNumberTextField.text = "\(UserDefaultManager.shared.currentUser?.phone ?? "0")"
        emailAddressTextField.text = "\(UserDefaultManager.shared.currentUser?.email ?? "om")"
    }
    
    func editAccount() {
        
        guard let username = userNameTextField.text, username.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال اسم المستخدم"
            showError(error: apiError)
            return
        }
        
        guard let myCurrentEmail = emailAddressTextField.text, myCurrentEmail.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let firstMobileNumber = firstPhoneNumberTextField.text, firstMobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let secondMobileNumber = secondPhoneNumberTextField.text, secondMobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let thirdMobileNumber = thirdPhoneNumberTextField.text, thirdMobileNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let landNumber = landPhoneNumberTextField.text, landNumber.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        if oldEmail == myCurrentEmail {
            
            let imgData = myImage!.jpegData(compressionQuality: 0.5)
            print(imgData)
            
            let parameters = [
                "first_name" : username as AnyObject,
                "phone" : landNumber as AnyObject,
                "mobile1" : firstMobileNumber as AnyObject,
                "mobile2" : secondMobileNumber as AnyObject,
                "mobile3" : thirdMobileNumber as AnyObject,
                "password" : password as AnyObject,
                ]
            
            startLoading()
            
            AuthenticationAPIManager().changeUserProfile(imageData: imgData,basicDictionary: parameters, onSuccess: { (newUser) in
             
                self.stopLoadingWithSuccess()
                UserDefaultManager.shared.currentUser = newUser
                self.showDonePopUp()


                
            }) { (error) in
                print(error)
            }

            
        } else if oldEmail != myCurrentEmail {
            
            let imgData = myImage!.pngData()
            
            let parameters = [
                "first_name" : username as AnyObject,
                "phone" : landNumber as AnyObject,
                "mobile1" : firstMobileNumber as AnyObject,
                "mobile2" : secondMobileNumber as AnyObject,
                "mobile3" : thirdMobileNumber as AnyObject,
                "email" : myCurrentEmail as AnyObject,
                "password" : password as AnyObject,
                ]
            
            startLoading()
            
            AuthenticationAPIManager().changeUserProfile(imageData: imgData!,basicDictionary: parameters, onSuccess: { (newUser) in
                
                self.stopLoadingWithSuccess()
                UserDefaultManager.shared.currentUser = newUser
                self.showDonePopUp()

                
            }) { (error) in
                print(error)
            } 

        }
        
        
        
        
    }
    
    //MARK:- API Calls
    func saveAdPictures() {
        
//        guard let postIDValue = postID else {
//            return
//        }
//        
//        let parameters: [String : AnyObject] = [
//            "postID" : postIDValue as AnyObject
//        ]
//        
//        var imageDataArr = [Data]()
//        for asset in assets {
//            let assetImage = getAssetThumbnail(asset: asset)
//            let imgData = assetImage.jpegData(compressionQuality: 1)
//            imageDataArr.append(imgData!)
//        }
//        
//        startLoading()
//        weak var weakSelf = self
//        AdvertisementsAPIManager().saveAdPictures(imageDataArray: imageDataArr, basicDictionary: parameters, onSuccess: {
//            
//            weakSelf?.stopLoadingWithSuccess()
//            weakSelf?.dismiss(animated: true, completion: nil)
//            
//        }) { (error) in
//            weakSelf?.stopLoadingWithError(error: error)
//        }
    }
    
    func showDonePopUp() {
        let alertController = UIAlertController(title: "لقد تم تغيير !", message: "معلومات الحساب", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "تم", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func returnToMyAccount() {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MyAcountNavigationController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func imagePressed() {
        
        let image = UIImagePickerController()
        
        image.delegate = self
        
        image.sourceType = .photoLibrary
        
        self.present(image, animated: true) {
            
        }
        
        
    }

    @IBAction func confirmAccountInfoButtonIsPressed(_ sender: Any) {
        print("Pressed")
        editAccount()
    }
    
    @IBAction func changeImageButtonIsPressed(_ sender: Any) {
        print("Photo button is pressed")
        imagePressed()
    }
    
    
}

extension EditAccountViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImage.image = image
            userImage.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
            myImage = image
            self.dismiss(animated: true, completion: nil)
            
        }
    }
}

extension EditAccountViewController: UITextFieldDelegate {
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//
//        print("70ssa")
//
//        let allowedCharacters = CharacterSet.decimalDigits
//        let characterSet = CharacterSet(charactersIn: string)
//        return allowedCharacters.isSuperset(of: characterSet)
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        print("70ssa")
        
        //        let allowedCharacters = CharacterSet.decimalDigits
        let allowedCharacters = CharacterSet(charactersIn:"0123456789١٢٣٤٥٦٧٨٩٠+")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
        func textFieldDidEndEditing(_ textField: UITextField) {
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    
}
