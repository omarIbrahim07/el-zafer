//
//  HomeViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/7/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: BaseViewController {
    
    var breakableChecked: Bool = false
    var encloseChecked: Bool = false
    var storeChecked: Bool = false
    
    var types: [Category] = []
    var typesTitles: [String] = []
    var typesIDs: [Int] = []
    
    var weightUnits: [String] = [
        "كجم",
        "لتر"
    ]
    
    var lengthUnits: [String] = [
        "متر",
        "بوصة"
    ]
    
    var ids: [Int] = [
        1,
        2
    ]
    
    var typeIdSelected: Int?
    var typeSelected: String?
    var weightUnitIdSelected: Int?
    var lengthUnitIdSelected: Int?
//    var weight: Float?
//    var height: Float?
//    var width: Float?
//    var longitude: Float?
    var weight: String?
    var height: String?
    var width: String?
    var longitude: String?
    
    var breakableId: Int? = 0
    var encloseId: Int? = 0
    var storeId: Int? = 0
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var longitudeTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var widthTextField: UITextField!
    @IBOutlet weak var longitudeUnitTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var weightUnitTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var breakableCheckedImage: UIImageView!
    @IBOutlet weak var encloseCheckedImage: UIImageView!
    @IBOutlet weak var storeCheckedImage: UIImageView!
    @IBOutlet weak var notificationsBarButton: UIBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        getTypes()
//        fireFireBase()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        revealViewController()
        // Do any additional setup after loading the view.
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        

    }
    
    @objc func viewTapped() {
        longitudeTextField.endEditing(true)
        heightTextField.endEditing(true)
        widthTextField.endEditing(true)
        weightTextField.endEditing(true)
        longitudeUnitTextField.endEditing(true)
        weightUnitTextField.endEditing(true)
        typeTextField.endEditing(true)
    }
    
    func configureView() {
                
        nextView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        changeTextFieldPlaceHolderColor(textField: typeTextField, placeHolderString: "النوع", fontSize: 15.0)
        changeTextFieldPlaceHolderColor(textField: weightTextField, placeHolderString: "الوزن", fontSize: 15.0)
        changeTextFieldPlaceHolderColor(textField: weightUnitTextField, placeHolderString: "الوحدة", fontSize: 11.0)
        changeTextFieldPlaceHolderColor(textField: widthTextField, placeHolderString: "العرض", fontSize: 11.0)
        changeTextFieldPlaceHolderColor(textField: heightTextField, placeHolderString: "الطول", fontSize: 11.0)
        changeTextFieldPlaceHolderColor(textField: longitudeTextField, placeHolderString: "الارتفاع", fontSize: 11.0)
        changeTextFieldPlaceHolderColor(textField: longitudeUnitTextField, placeHolderString: "الوحدة", fontSize: 11.0)
        typeTextField.setIcon(UIImage(named: "01_0003_delivery-package-opened")!, imageState: "Home")
        weightTextField.setIcon(UIImage(named: "weight")!, imageState: "Home")
        
        createPicker(textField: typeTextField, tag: 0)
        createPicker(textField: weightUnitTextField, tag: 1)
        createPicker(textField: longitudeUnitTextField, tag: 2)
        
        heightTextField.delegate = self
        weightTextField.delegate = self
        longitudeTextField.delegate = self
        weightTextField.delegate = self
        widthTextField.delegate = self
        

        notificationsBarButton.target = self
        notificationsBarButton.action = #selector(goToNotifications)
    }
    
    @objc func goToNotifications() {
        if let myNotificationsNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "notificationsNavigationController") as? UINavigationController, let rootViewContoller = myNotificationsNavigationController.viewControllers[0] as? NotificationsViewController {
            //                rootViewContoller.test = "test String"
            self.present(myNotificationsNavigationController, animated: true, completion: nil)
        }
    }
    
    
    func fireFireBase() {
        
//        var ref: DatabaseReference!

        let ref = Database.database().reference(fromURL: "https://elzafer-f4332.firebaseio.com/")
        
        let userRefrence = ref.child("Orders").child("4")
        let values = ["longitude" : 5.2, "latitude" : 6.3, "Status" : 1]
        
        userRefrence.updateChildValues(values) { (err, ref) in
            
            if err != nil {
                print(err as Any)
                return
            }
            
            print("Successfully")
            
        }
    }
    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        // for just placeholder font color
//        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString,
//                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        // for placeholder font color and font bold
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6), .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }
    
    func getTypes() {
        
        self.startLoading()
        
        PackageAPIManager().getTypes(basicDictionary: [:], onSuccess: { (types) in
            
            self.types = types
            
            for category in self.types {
                self.typesTitles.append(category.name)
                self.typesIDs.append(category.id)
            }
            
            self.typeSelected = types[0].name
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }
    
    func createPicker(textField: UITextField,tag: Int) {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        textField.inputView = Picker
        Picker.tag = tag
    }
    
    @IBAction func nextButtonIsPressed(_ sender: Any) {
        
        guard let type = typeTextField.text, type.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال نوع الشحنة"
            showError(error: apiError)
            return
        }
        
        guard let weight = weightTextField.text, weight.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال وزن الشحنة"
            showError(error: apiError)
            return
        }
        
        guard let weightUnit = weightUnitTextField.text, weightUnit.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال وحدة وزن الشحنة"
            showError(error: apiError)
            return
        }
        
        guard let height = heightTextField.text, height.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الطول"
            showError(error: apiError)
            return
        }
        
        guard let width = widthTextField.text, width.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال العرض"
            showError(error: apiError)
            return
        }
        
        guard let longitude = longitudeTextField.text, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال الارتفاع"
            showError(error: apiError)
            return
        }
        
        guard let heightUnit = longitudeUnitTextField.text, heightUnit.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء إدخال وحدة طول الشحنة"
            showError(error: apiError)
            return
        }
        
//        self.weight = Float(weight)
//        self.height = Float(height)
//        self.width = Float(width)
//        self.longitude = Float(longitude)
        self.weight = weight
        self.height = height
        self.width = width
        self.longitude = longitude

        
        
        print("pressed")
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SelectDestViewController") as! SelectDestViewController
        
        viewController.typeIdSelected = self.typeIdSelected
        viewController.weightUnitIdSelected = self.weightUnitIdSelected
        viewController.weight = self.weight
        viewController.height = self.height
        viewController.width = self.width
        viewController.longitude = self.longitude
        viewController.lengthUnitIdSelected = self.lengthUnitIdSelected
        viewController.breakableId = self.breakableId
        viewController.encloseId = self.encloseId
        viewController.storeId = self.storeId
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    @IBAction func brokenButtonIsPressed(_ sender: Any) {
        if breakableChecked == false {
            breakableCheckedImage.image = UIImage(named: "Checked")
            breakableChecked = true
            breakableId = 1
            print(breakableId)
            print("Breakable")
        } else if breakableChecked == true {
            breakableCheckedImage.image = UIImage(named: "01_0004_Rounded-Rectangle-8")
            breakableChecked = false
            breakableId = 0
            print(breakableId)
            print("non Breakable")
        }
    }
    
    @IBAction func encloseButtonIsPressed(_ sender: Any) {
        if encloseChecked == false {
            encloseCheckedImage.image = UIImage(named: "Checked")
            encloseChecked = true
            encloseId = 1
            print(encloseId)
            print("enclose")
        } else if encloseChecked == true {
            encloseCheckedImage.image = UIImage(named: "01_0004_Rounded-Rectangle-8")
            encloseChecked = false
            encloseId = 0
            print(encloseId)
            print("don't enclose")
        }
    }
    
    @IBAction func storeButtonIsPressed(_ sender: Any) {
        if storeChecked == false {
            storeCheckedImage.image = UIImage(named: "Checked")
            storeChecked = true
            storeId = 1
            print(storeId)
            print("store")
        } else if storeChecked == true {
            storeCheckedImage.image = UIImage(named: "01_0004_Rounded-Rectangle-8")
            storeChecked = false
            storeId = 0
            print(storeId)
            print("don't store")
        }
    }
    
    
}

extension HomeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        print("70ssa")
        
//        let allowedCharacters = CharacterSet.decimalDigits
//        let allowedCharacters = CharacterSet(charactersIn:".0123456789١٢٣٤٥٦٧٨٩٠,")
        let allowedCharacters = CharacterSet(charactersIn:".0123456789")
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension HomeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return self.typesTitles.count
        } else if pickerView.tag == 1 {
            return self.weightUnits.count
        } else if pickerView.tag == 2 {
            return self.lengthUnits.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            typeTextField.text = typesTitles[0]
            typeIdSelected = typesIDs[0]
            return self.typesTitles[row]
        } else if pickerView.tag == 1 {
            weightUnitTextField.text = weightUnits[0]
            weightUnitIdSelected = ids[0]
            return self.weightUnits[row]
        } else if pickerView.tag == 2 {
            longitudeUnitTextField.text = lengthUnits[0]
            lengthUnitIdSelected = ids[0]
            return self.lengthUnits[row]
        }
        return ""
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            typeTextField.text = typesTitles[row]
            typeIdSelected = typesIDs[row]
        } else if pickerView.tag == 1 {
            weightUnitTextField.text = weightUnits[row]
            weightUnitIdSelected = ids[row]
        } else if pickerView.tag == 2 {
            longitudeUnitTextField.text = lengthUnits[row]
            lengthUnitIdSelected = ids[row]
        }
//            self.view.endEditing(true)
    }
    
}
