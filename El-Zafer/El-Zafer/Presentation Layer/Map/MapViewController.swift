//
//  MapViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/14/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class MapViewController: BaseViewController {
    
    var latitude: Double?
    var longitude: Double?
    var status: Int?
    
    var orderId: Int?
    
    // You don't need to modify the default init(nibName:bundle:) method.
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!
    
    func congi(latitude: Double, longitude: Double) {
//        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 13.0)
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude), zoom: 19.0)
        self.mapView.camera = camera
        
        let marker = GMSMarker()
//        marker.position = position
        marker.map = mapView
    }
    
    override func viewDidLoad() {
        navigationItem.title = "تتبُع الشُحنة"
        mapView.delegate = self
        getFirebaseData()
//        congi()
    }
    
    func getFirebaseData() {
        
        if let OrderId = self.orderId {
            Database.database().reference().child("Orders").child(String(OrderId)).observe(.value, with: { (snapshot) in
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                
                print("Besm allah")
                
                if let lat = postDict["latitude"] as? Double, let long = postDict["longitude"] as? Double, let status = postDict["Status"] as? Int {
                    //                self.latitudeLabel.text = String(lat)
                    //                self.longitudeLabel.text = String(long)
                    self.status = status
                    self.longitude = long
                    self.latitude = lat
                    
                    if status == 1 {
                        self.congi(latitude: lat, longitude: long)
                    } else {
                        self.showNoTrackingPopUp()
                    }
                } else {
                    self.showNoTrackingPopUp()
                }
            })
        }
    
    }
    
    ////////////////////////////////////////////
    // for single event only
    
    //    func getFirebaseData() {
    //        Database.database().reference().child("Orders").child("9").observe(of: .value, with: { (snapShot) in
    //
    //            if let dictionary = snapShot.value as? [String : AnyObject] {
    //
    //                if let lat = dictionary["latitude"] as? Int, let long = dictionary["longitude"] as? Int {
    //                    self.latitLabel.text = String(lat)
    //                    self.longitudeLabel.text = String(long)
    //                }
    //
    ////                self.latitLabel.text = dictionary["latitude"] as? String
    ////                self.longitudeLabel.text = dictionary["longitude"] as? String
    //            }
    //
    //            print(snapShot)
    //
    //        }, withCancel: nil)
    //    }
    
    //////////////////////////////////////////
    
    func showNoTrackingPopUp() {
        let alertController = UIAlertController(title: "عُذرًا الشُحنة لا تتحرك الآن !", message: "يمكنك تتبُع الشُحنة عند تحركها", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "الذهاب إلى الرئيسية", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController

    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        
        // 1
        let geocoder = GMSGeocoder()
        
        // 2
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            
            // 3
            self.addressLabel.text = lines.joined(separator: "\n")
            
            // 4
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func currentTrackingLocationIsPressed(_ sender: Any) {
        print("7obbbbbbb")
        getFirebaseData()
    }
    
    
}

// MARK: - GMSMapViewDelegate
extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("5amorgia")
        reverseGeocodeCoordinate(position.target)
        
        if let lat = self.latitude, let long = self.longitude {
            self.congi(latitude: lat, longitude: long)
        }
    }
    
}
