//
//  FollowPackageViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/12/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps

class FollowPackageViewController: BaseViewController {

    var trackedIds: [OrderId] = []
    var stringTrackedIds: [String] = []
    var typeIdSelected: Int?
    
    @IBOutlet weak var followPackageView: UIView!
    @IBOutlet weak var trackedIdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        getTrackedPackages()
                
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        trackedIdTextField.endEditing(true)
    }
    
    func configureView() {
        followPackageView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        createPicker(textField: trackedIdTextField, tag: 0)
    }
    
    func createPicker(textField: UITextField,tag: Int) {
        
        let Picker = UIPickerView()
        Picker.delegate = self
        
        textField.inputView = Picker
        Picker.tag = tag
    }
    
    func getTrackedPackages() {
        
        let parameters: [String : AnyObject] = [ "service_id" : 3 as AnyObject ]
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().getTrackedPackages(basicDictionary: parameters, onSuccess: { (packagesIds) in
            
            self.stopLoadingWithSuccess()
            self.trackedIds = packagesIds

            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
            self.showNoTrackingPopUp()
        }
    }
    
    func showNoTrackingPopUp() {
        let alertController = UIAlertController(title: "عُذرًا لا يوجد لديك شُحنات يمكن تتبُعها !", message: "العودة إلى الرئيسية", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let openAction = UIAlertAction(title: "إغلاق", style: .default) { (action) in
            self.goToHomePage()
        }
        alertController.addAction(openAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func goToHomePage() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
        UIApplication.shared.keyWindow?.rootViewController = viewController
        
    }
    
    func goToMap() {
        let storyboard = UIStoryboard.init(name: "Menu", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        viewController.orderId = self.typeIdSelected
        navigationController?.pushViewController(viewController, animated: true)
    }

    @IBAction func followButtonIsPressed(_ sender: Any) {
        guard let orderId = self.typeIdSelected, orderId > 0 else {
            let apiError = APIError()
            apiError.message = "من فضلك اختر رقم الشُحنة"
            showError(error: apiError)
            return
        }
        print("Follow")
        goToMap()
    }
    
}

extension FollowPackageViewController: UIPickerViewDelegate, UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return self.trackedIds.count > 0 ? 1 : 0
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return self.trackedIds.count
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            trackedIdTextField.text = String(trackedIds[0].id)
            typeIdSelected = trackedIds[0].id
            return String(self.trackedIds[row].id)
        }
        return ""
    }



    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if pickerView.tag == 0 {
            trackedIdTextField.text = String(trackedIds[row].id)
            typeIdSelected = trackedIds[row].id
        }
        //            self.view.endEditing(true)
    }

}

extension FollowPackageViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
