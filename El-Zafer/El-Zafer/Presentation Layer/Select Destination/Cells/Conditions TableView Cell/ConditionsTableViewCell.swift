//
//  ConditionsTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/20/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class ConditionsTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var conditionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
