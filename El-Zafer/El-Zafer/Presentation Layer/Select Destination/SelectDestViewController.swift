//
//  SelectDestViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/8/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class SelectDestViewController: BaseViewController {
    
    var conditionsChecked: Bool = false
    var conditionCheckedInt: Int? = 0
    var choosed: Int? = 0
    var choosedString: String? = ""
    
    var typeIdSelected: Int?
    var weightUnitIdSelected: Int?
    var lengthUnitIdSelected: Int?
//    var weight: Float?
//    var height: Float?
//    var width: Float?
//    var longitude: Float?
    var weight: String?
    var height: String?
    var width: String?
    var longitude: String?
    
    var breakableId: Int?
    var encloseId: Int?
    var storeId: Int?
    var deliveryMethodId: Int?

    var packageId: Int?
    
    var deliveryMethods: [DeliveryMethod] = []
    let conditionError = APIError()
    
    @IBOutlet weak var selectDestinationView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var conditionsView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var conditionsTableView: UITableView!
    @IBOutlet weak var conditionCheckImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "مكان التسليم والتسلُم"
        configureView()
        configureTableView()
        getDeliveryMethodsAndConditions()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "SelectDestinationTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectDestinationTableViewCell")
        conditionsTableView.register(UINib(nibName: "ConditionsTableViewCell", bundle: nil), forCellReuseIdentifier: "ConditionsTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
        conditionsTableView.dataSource = self
        conditionsTableView.delegate = self
        conditionsTableView.reloadData()
    }
    
    func configureView() {
        selectDestinationView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
        conditionsView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
        nextView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
        tableView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 2)
        conditionsTableView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        tableView.separatorInset = .zero
    }
    
    func getDeliveryMethodsAndConditions() {
        
        self.startLoading()
        
        PackageAPIManager().getDeliveryMethods( basicDictionary: [:], onSuccess: { (deliveryMethods) in
            
            self.deliveryMethods = deliveryMethods
            self.tableView.reloadData()
            self.conditionsTableView.reloadData()
            
            self.stopLoadingWithSuccess()
            
        }) { (error) in
            print("error")
        }
    }
    
    func sendPackage() {
        
//        guard let longitude = self.longitude, longitude > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال اسم المستخدم"
//            showError(error: apiError)
//            return
//        }
//
//        guard let width = self.width, width > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال البريد الالكتروني"
//            showError(error: apiError)
//            return
//        }
//
//        guard let height = self.height, height > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
//
//        guard let weight = self.weight, weight > 0 else {
//            let apiError = APIError()
//            apiError.message = "برجاء ادخال رقم الجوال"
//            showError(error: apiError)
//            return
//        }
        guard let longitude = self.longitude, longitude.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال اسم المستخدم"
            showError(error: apiError)
            return
        }
        
        guard let width = self.width, width.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let height = self.height, height.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let weight = self.weight, weight.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let heightDimension = self.lengthUnitIdSelected, heightDimension > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let weightUnit = self.weightUnitIdSelected, weightUnit > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال رقم الجوال"
            showError(error: apiError)
            return
        }
        
        guard let type = self.typeIdSelected, type > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let breakable = self.breakableId, breakable >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let packaging = self.encloseId, packaging >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور"
            showError(error: apiError)
            return
        }
        
        guard let storing = self.storeId, storing >= 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال تأكيد كلمة المرور المُراد"
            showError(error: apiError)
            return
        }
        
        guard let conditionCheck = self.conditionCheckedInt, conditionCheck == 1 else {
            let apiError = APIError()
            apiError.message = "برجاء الموافقة على شروط التعاقُد"
            showError(error: apiError)
            return
        }
        
        
        
        let orderStatus = "1"
        let ostatus = "0"
        
        let parameters = [
            "length" : self.height as AnyObject,
            "width" : self.width as AnyObject,
            "height" : self.longitude as AnyObject,
            "dim_unit" : self.lengthUnitIdSelected as AnyObject,
            "weight" : self.weight as AnyObject,
            "weight_unit" : self.weightUnitIdSelected as AnyObject,
            "category_id" : self.typeIdSelected as AnyObject,
            "deliveryMethod_id" : self.deliveryMethodId as AnyObject,
            "breakable" : self.breakableId as AnyObject,
            "packaging" : self.encloseId as AnyObject,
            "storage" : self.storeId as AnyObject,
            "orderStatus_id" : orderStatus as AnyObject,
            "ostatus" : ostatus as AnyObject,
        ]
        
        weak var weakSelf = self
        
        startLoading()
        
        PackageAPIManager().addPackage(basicDictionary: parameters, onSuccess: { (packageIdd) in
            
            self.stopLoadingWithSuccess()
            print(packageIdd)
            
            self.packageId = packageIdd
            self.goToMap()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func goToMap() {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "PickDestinationViewController") as! PickDestinationViewController
        
        viewController.storeId = self.storeId
        
        viewController.controllerName = self.choosedString
        viewController.choosed = self.choosed
        
        viewController.packageID = self.packageId
        
        navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func agreeOnConditionsIsPressed(_ sender: Any) {
        if conditionsChecked == false {
            conditionCheckImage.image = UIImage(named: "Checked")
            conditionsChecked = true
            conditionCheckedInt = 1
            print("checked")
        } else if conditionsChecked == true {
            conditionCheckImage.image = UIImage(named: "01_0004_Rounded-Rectangle-8")
            conditionsChecked = false
            conditionCheckedInt = 0
            print("unchecked")
        }
    }
    
    
    @IBAction func nextIsPressed(_ sender: Any) {
        
        self.deliveryMethodId = choosed
        
        guard let deliverMethod = self.deliveryMethodId, deliverMethod > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء اختيار طريقة التوصيل"
            showError(error: apiError)
            return
        }
        
        if deliverMethod > 0 {
            sendPackage()
        }
    }
    
    
}

extension SelectDestViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
//            return menuElements.count
            return self.deliveryMethods.count
        } else if tableView == self.conditionsTableView {
            return self.deliveryMethods.count > 0 ? 1 : 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableView {
            if let cell: SelectDestinationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SelectDestinationTableViewCell") as? SelectDestinationTableViewCell {
                cell.selectDestinationLabel.text = self.deliveryMethods[indexPath.row].name
                return cell
            }
        } else if tableView == self.conditionsTableView {
            if let cell: ConditionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ConditionsTableViewCell") as? ConditionsTableViewCell {
                cell.conditionLabel.text = self.deliveryMethods[indexPath.row].termsOfContract
                return cell
            }
        }
    
        return UITableViewCell()
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            choosed = 1
//        } else if indexPath.row == 1 {
//            choosed = 2
//        } else if indexPath.row == 2 {
//            choosed = 3
//        } else if indexPath.row == 3 {
//            choosed = 4
//        }
//    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            choosed = 1
            self.choosedString = deliveryMethods[0].name
        } else if indexPath.row == 1 {
            choosed = 2
            self.choosedString = deliveryMethods[1].name
        } else if indexPath.row == 2 {
            choosed = 3
            self.choosedString = deliveryMethods[2].name
        } else if indexPath.row == 3 {
            choosed = 4
            self.choosedString = deliveryMethods[3].name
        }
    }
    
}
