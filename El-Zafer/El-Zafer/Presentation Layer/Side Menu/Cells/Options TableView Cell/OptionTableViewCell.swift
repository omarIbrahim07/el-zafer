//
//  OptionTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/7/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class OptionTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var optionTitleLabel: UILabel!
    @IBOutlet weak var menuCellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        selectionStyle = .none
        configureUI()
        // Initialization code
    }

    func configureUI() {
        view.backgroundColor = #colorLiteral(red: 0.2392156863, green: 0.2392156863, blue: 0.2392156863, alpha: 1)
        view.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1, green: 1, blue: 0, alpha: 1), borderWidth: 2)
    }
    
    
}
