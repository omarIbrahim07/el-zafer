//
//  LogOutTableViewCell.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/9/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class LogOutTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
