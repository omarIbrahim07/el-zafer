//
//  SideMenuViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/7/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class SideMenuViewController: BaseViewController {
    
    var menuElements: [String] = ["الرئيسية",
                                  "حسابي",
                                  "شُحناتي",
                                  "تتبُع الشحنة",
                                  "شكاوى ومقترحات",
                                  "حول البرنامج",
"تقييم الخدمة",
"تقييم المندوب",
"مشاركة الأصدقاء"
    ]
    
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        setUserData()
        configureTableView()

        
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "OptionTableViewCell", bundle: nil), forCellReuseIdentifier: "OptionTableViewCell")
        tableView.register(UINib(nibName: "LogOutTableViewCell", bundle: nil), forCellReuseIdentifier: "LogOutTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }
    
    func configureView() {
        userImage.addCornerRadius(raduis: userImage.frame.height / 2, borderColor: #colorLiteral(red: 1, green: 1, blue: 0, alpha: 1), borderWidth: 2)
        
        if let image = UserDefaultManager.shared.currentUser?.image {
            let imageURL = ImageURL + image
            userImage.loadImageFromUrl(imageUrl: imageURL)
        }
    }
    
    func setUserData() {
        userNameLabel.text = UserDefaultManager.shared.currentUser?.firstName
        userEmailLabel.text = UserDefaultManager.shared.currentUser?.email
    }
    
    func shareAppStoreURL() {
        if let name = NSURL(string: "https://itunes.apple.com/us/app/myapp/idxxxxxxxx?ls=1&mt=8") {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }
        else
        {
            // show alert for not available
        }
    }
    
    func logout() {
        
        self.startLoading()
        
        AuthenticationAPIManager().logout(basicDictionary: [:], onSuccess: { (message) -> String in
            
            self.stopLoadingWithSuccess()
            self.goToLogIn()
            
            return "70bby"
            
        }) { (error) in
            print(error)
        }
    }
    
    func goToLogIn() {
        if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "SignInNavigationController") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {

            UIApplication.shared.keyWindow?.rootViewController = signInNavigationController
        }
    }
    
    func goToDriverEvaluation() {
        if let driverEvaluateNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "DriverEvaluationNavigationViewController") as? UINavigationController, let rootViewContoller = driverEvaluateNavigationController.viewControllers[0] as? DriverEvaluationViewController {
            //                rootViewContoller.test = "test String"
            self.present(driverEvaluateNavigationController, animated: true, completion: nil)
            print("Fes")
        }
    }
    
    deinit {
        print("SideMenuViewController deinit")
    }

}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuElements.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    if indexPath.row <= 8 {
    if let cell: OptionTableViewCell = tableView.dequeueReusableCell(withIdentifier: "OptionTableViewCell") as? OptionTableViewCell {
        if indexPath.row == 8 {
            cell.optionTitleLabel.text = menuElements[indexPath.row]
            cell.menuCellImage.image = UIImage(named: "01_0000_share-copy")
        } else {
            cell.optionTitleLabel.text = menuElements[indexPath.row]
            }
            
            return cell
        }
    } else if indexPath.row == 9 {
            if let cell: LogOutTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LogOutTableViewCell") as? LogOutTableViewCell {
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // To free side menu after choosing a cell
        self.revealViewController().revealToggle(animated: true)
        
        if indexPath.row == 0 {
            
//            if let homeNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeNavigationViewController") as? UINavigationController, let rootViewContoller = homeNavigationController.viewControllers[0] as? HomeViewController {
//                //                rootViewContoller.test = "test String"
//                self.present(homeNavigationController, animated: true, completion: nil)
//                print("Fes")
//            }
        } else if indexPath.row == 1 {
            
            if let myAccountNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyAcountNavigationController") as? UINavigationController, let rootViewContoller = myAccountNavigationController.viewControllers[0] as? MyAccountViewController {
                //                rootViewContoller.test = "test String"
                self.present(myAccountNavigationController, animated: true, completion: nil)
                print("Fes")
            }
        } else if indexPath.row == 2 {
            
            if let myPackagesNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "MyPackagesNavigationViewController") as? UINavigationController, let rootViewContoller = myPackagesNavigationController.viewControllers[0] as? MyPackagesViewController {
                //                rootViewContoller.test = "test String"
                self.present(myPackagesNavigationController, animated: true, completion: nil)
                print("Fes")
                
            }
        } else if indexPath.row == 3 {

            if let followPackageNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "FollowPackageNavigationViewController") as? UINavigationController, let rootViewContoller = followPackageNavigationController.viewControllers[0] as? FollowPackageViewController {
                //                rootViewContoller.test = "test String"
                self.present(followPackageNavigationController, animated: true, completion: nil)
                print("Fes")

            }
        } else if indexPath.row == 4 {
            
            if let complainsNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ComplainsNavigationViewController") as? UINavigationController, let rootViewContoller = complainsNavigationController.viewControllers[0] as? ComplainsViewController {
                //                rootViewContoller.test = "test String"
                self.present(complainsNavigationController, animated: true, completion: nil)
                print("Fes")
                
            }
        } else if indexPath.row == 5 {
            
            if let aboutNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "AboutNavigationViewController") as? UINavigationController, let rootViewContoller = aboutNavigationController.viewControllers[0] as? AboutViewController {
                //                rootViewContoller.test = "test String"
                self.present(aboutNavigationController, animated: true, completion: nil)
                print("Fes")
                
            }
        } else if indexPath.row == 6 {
            
            if let evaluateNavigationController = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "EvaluationsNavigationViewController") as? UINavigationController, let rootViewContoller = evaluateNavigationController.viewControllers[0] as? EvaluationsViewController {
                //                rootViewContoller.test = "test String"
                self.present(evaluateNavigationController, animated: true, completion: nil)
                print("Fes")
                
            }
        } else if indexPath.row == 7 {
                self.goToDriverEvaluation()
        } else if indexPath.row == 8 {
            self.shareAppStoreURL()
            print("Share")
            
        } else if indexPath.row == 9 {
            self.logout()
            
//            UserDefaultManager.shared.authorization = nil
//            if let signInNavigationController = storyboard?.instantiateViewController(withIdentifier: "SignInNavigationController") as? UINavigationController, let loginViewController = signInNavigationController.viewControllers[0] as? LoginViewController {
//
//                UIApplication.shared.keyWindow?.rootViewController = loginViewController
//            }
        }
            
//            let storyboard : UIStoryboard = UIStoryboard(name: "Menu", bundle: nil)
//            let vc : FollowPackageViewController = storyboard.instantiateViewController(withIdentifier: "FollowPackageViewController") as! FollowPackageViewController
////            vc.teststring = "hello"
//
//            let navigationController = UINavigationController(rootViewController: vc)
//
//            self.present(navigationController, animated: true, completion: nil)
//            print("Fes")
//        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
