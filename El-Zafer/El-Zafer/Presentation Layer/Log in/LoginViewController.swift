//
//  LoginViewController.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/15/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signInWithAnotherAccountButton: UIButton!
    @IBOutlet weak var newAccountView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "تسجيل الدخول"
        configureView()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    @objc func viewTapped() {
        emailTextField.endEditing(true)
        passwordTextField.endEditing(true)
        
    }
    
    func configureView() {
        loginButton.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 1)
        signInWithAnotherAccountButton.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 1)
        newAccountView.addCornerRadius(raduis: 15, borderColor: #colorLiteral(red: 0.3899102211, green: 0.7214819789, blue: 0.4274870157, alpha: 1), borderWidth: 0)
    }
    
    func loginUser() {
        
        guard let email = emailTextField.text, email.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال البريد الالكتروني"
            showError(error: apiError)
            return
        }
        
        guard let password = passwordTextField.text, password.count > 0 else {
            let apiError = APIError()
            apiError.message = "برجاء ادخال كلمة المرور"
            showError(error: apiError)
            return
        }
        
        let parameters = [
            "email" : email as AnyObject,
            "password" : password as AnyObject,
            "fcm_token" : FirebaseToken as AnyObject,
            ]
        
        weak var weakSelf = self
        weakSelf?.startLoading()
        AuthenticationAPIManager().loginUser(basicDictionary: parameters, onSuccess: { (_) in
            
            weakSelf?.getUserProfile()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    func getUserProfile() {
        weak var weakSelf = self
        
        AuthenticationAPIManager().getUserProfile(onSuccess: { (_) in
            
            weakSelf?.stopLoadingWithSuccess()
            weakSelf?.presentHomeScreen()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
    }
    
    //MARK:- Navigation
    func presentHomeScreen(isPushNotification: Bool = false) {
        if let _ = UserDefaultManager.shared.currentUser, let _ = UserDefaultManager.shared.authorization {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
            appDelegate.window!.rootViewController = viewController
            appDelegate.window!.makeKeyAndVisible()
        }
    }
    
    func presentRegistration() {
        if let SignUpNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationSignUpViewController") as? UINavigationController, let rootViewContoller = SignUpNavigationController.viewControllers[0] as? SignUpViewController {
            //                rootViewContoller.test = "test String"
            self.present(SignUpNavigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func logInButtonIsPressed(_ sender: Any) {
        print("Logged in")
        loginUser()
    }
    
    @IBAction func forgetPasswordButtonIsPressed(_ sender: Any) {
        print("Forgot Password")
    }
    
    @IBAction func facebookAccountButtonIsPressed(_ sender: Any) {
        print("Facebook pressed")
    }
    
    @IBAction func gmailButtonIsPressed(_ sender: Any) {
        print("Gmail is pressed")
    }
    
    @IBAction func newAccountButtonIsPressed(_ sender: Any) {
        print("New Account is pressed")
        presentRegistration()
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}
